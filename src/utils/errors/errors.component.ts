import { Component, OnInit } from '@angular/core';
import { ErrorInputService } from '../../services/modules_services/error_input.service';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.css'],
  providers: [ErrorInputService]
})
export class ErrorsComponent implements OnInit {

  error_mensagem : string = ErrorInputService.mensagem_erro

  constructor() { }

  ngOnInit() {
    
  }

}
