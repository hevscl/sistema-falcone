import { Component, OnInit } from '@angular/core';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';

@Component({
  selector: 'app-responses',
  templateUrl: './responses.component.html',
  styleUrls: ['./responses.component.css'],
  providers: [ResponsesModuleService]
})
export class ResponsesComponent implements OnInit {

  public mensagem : string = ResponsesModuleService.mensagem
  public close_button : string = "Ok, Entendi"

  public altura : string = ResponsesModuleService.altura

  public styleResponse = {
    'height': this.altura
  }

  public response_code : number = ResponsesModuleService.code

  constructor(private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
  }

  fecharResponseScreen(status: boolean){
    this.responsesModuleService.fecharResponse(status)
  }

}
