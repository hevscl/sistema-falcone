export class Constants {


    constructor(){
        var API_URL : string
        var API_IMAGENS : string
    }

    //PROD
    static API_URL = 'https://api.falconesuplementos.com:8080/sis_web/'

    static API_IMAGENS = 'https://static.netshoes.com.br/produtos/'
    static API_IMAGENS_PRODUTOS = 'https://api.falconesuplementos.com:8080/imagens/produtos/'
    static API_IMAGENS_PROFISSIONAIS = 'https://api.falconesuplementos.com:8080/imagens/profissional/'
    
    //HOMOLOG

    //static API_URL = 'https://apifalconeselect.herokuapp.com/sis_web/'
    
}