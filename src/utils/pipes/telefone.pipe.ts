import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'telefonePipe'
})
export class TelefonePipe implements PipeTransform{
    transform(texto: string): string {
        if(texto.length===10){
            return "("
            + texto.substr(0,2)
            + ')' + texto.substr(2,4)
            + '-' + texto.substr(6,4)
        }
        return texto
    }
}