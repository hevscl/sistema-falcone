import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'cepPipe'
})
export class CepPipe implements PipeTransform{
    transform(texto: string): string {
        if(texto.length===8){
            return texto.substr(0,2) 
            + '.' + texto.substr(2,3)
            + '-' + texto.substr(5,3)
        }
        return texto
    }
}