import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'celularPipe'
})
export class CelularPipe implements PipeTransform{
    transform(texto: string): string {
        if(texto.length===11){
            return "("
            + texto.substr(0,2)
            + ')' + texto.substr(2,5)
            + '-' + texto.substr(7,4)
        }
        return texto
    }
}