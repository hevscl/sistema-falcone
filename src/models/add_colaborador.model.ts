export class AddColaborador {
    constructor(
        public perfil_colaborador: string, 
        public id_unidade: number,
        public nome: string,
        public cpf: number,
        public email: string){
    }

}