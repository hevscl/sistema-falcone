export class Transferencia {
    constructor(public id_unidade: number, 
        public banco: string, 
        public agencia: number,
        public conta: string, 
        public tipo_conta: string,
        public valor: string ){
    }
}