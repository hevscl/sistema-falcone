export class Cadastro {
    constructor(
        public tipo: string,
        public nome_e_sobrenome: string, 
        public email: string, 
        public celular: string,
        public cpf: string,
        public cnpj: string,
        public razaosocial: string,
        public fantasia: string,
        public senha: string,
        public cep: string, 
        public endereco: string, 
        public numero: string,
        public bairro: string, 
        public cidade: string,
        public estado: string,
        public codigo_proposta: string,
        public img: string ){
    }
}