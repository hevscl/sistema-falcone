export class DadosAdmin {
    constructor(
        public nome_admin: string, 
        public email_admin: string,
        public cpf_admin: number,
        public celular_admin: number,
        public telefone_empresa: number){
    }
}