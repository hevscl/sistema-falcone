import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class AcessPagesService {

    static acess_pages : any

    static emitirAcessPagesConfig = new EventEmitter<any>()

    enviaAcessPagesConfig(acess_pages :any){
        AcessPagesService.emitirAcessPagesConfig.emit(acess_pages)
        AcessPagesService.acess_pages = acess_pages
    }

}