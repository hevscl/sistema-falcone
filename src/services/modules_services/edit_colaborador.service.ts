import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class EditColaboradorService {

    static info_colaborador : any

    static emitirAcessPagesConfig = new EventEmitter<any>()

    enviaInfoColaboradorConfig(info_colaborador :any){
        EditColaboradorService.emitirAcessPagesConfig.emit(info_colaborador)
        EditColaboradorService.info_colaborador = info_colaborador
    }

}