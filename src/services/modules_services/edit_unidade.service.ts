import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class EditUnidadeService {

    static info_unidades : any

    static emitirAcessPagesConfig = new EventEmitter<any>()

    enviaInfoUnidadesConfig(info_unidades :any){
        EditUnidadeService.emitirAcessPagesConfig.emit(info_unidades)
        EditUnidadeService.info_unidades = info_unidades
    }

}