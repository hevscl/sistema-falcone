import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class BancoSelectedService {

    static banco_selected : string

    static emitirBancoSelected = new EventEmitter<String>()

    enviaBancoSelecionado(banco_selecionado :string){
        BancoSelectedService.emitirBancoSelected.emit(banco_selecionado)
    }

}