import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class AcessoService {

    static estadoFundo : string = "criado"
    static estadoC2 : string = "vazio"
    static estadoC3 : string = "vazio"

    static emitirEstadoFundo = new EventEmitter<String>()
    static emitirEstadoC2 = new EventEmitter<String>()
    static emitirEstadoC3 = new EventEmitter<String>()

    static enviaEstadoFundo(estado_fundo :string){
        AcessoService.emitirEstadoFundo.emit(estado_fundo)
    }

    static enviaEstadoC2(estado_c2 :string){
        AcessoService.emitirEstadoC2.emit(estado_c2)
    }

    static enviaEstadoC3(estado_c3 :string){
        AcessoService.emitirEstadoC3.emit(estado_c3)
    }

}