import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class ResponsesModuleService {

    static emitirMensagem = new EventEmitter<String>()

    static emitirFecharResponseModule = new EventEmitter<Boolean>()

    static code : number
    static mensagem : string
    static altura : string

    enviaConfigResponse(code: number, mensagem :string, altura :string){
        ResponsesModuleService.code = code
        ResponsesModuleService.mensagem = mensagem
        ResponsesModuleService.altura = altura
        ResponsesModuleService.emitirMensagem.emit(mensagem)
    }

    fecharResponse(status: boolean){
        ResponsesModuleService.emitirFecharResponseModule.emit(status)
    }

}