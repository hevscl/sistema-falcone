import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class ColaboradoresSelectedService {

    static emitirRefreshColaboradores = new EventEmitter<Boolean>()

    refreshColaboradores(refresh :boolean){
        ColaboradoresSelectedService.emitirRefreshColaboradores.emit(refresh);
    }

}