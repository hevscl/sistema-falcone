import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class EditProfissionalService {

    static profissional : any

    static emitirProfissional = new EventEmitter<any>()

    enviaEditProfissional(profissional :any){
        EditProfissionalService.profissional = profissional
    }

}