import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class OperadorSelectedService {

    operador_selected : any

    static id_unidade_selected : number

    static emitirUnidadeSelecionada = new EventEmitter<number>()

    static emitirOperadorSelecionado = new EventEmitter<any>()

    enviaOperadorSelecionado(operador_selected :any){
        this.operador_selected = operador_selected
        OperadorSelectedService.emitirOperadorSelecionado.emit(operador_selected)
    }

    enviaUnidadeSelecionada(id_unidade_selected: number){
        OperadorSelectedService.id_unidade_selected = id_unidade_selected
        OperadorSelectedService.emitirUnidadeSelecionada.emit(id_unidade_selected)
    }
}