import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class ListaProdutoService {

    static produtos : any
    static receita : any

    static emitirProduto = new EventEmitter<any>()

    enviaProduto(produto :any){
        ListaProdutoService.produtos = produto
    }

    enviaReceita(receita :any){
        ListaProdutoService.receita = receita
        console.log(receita);
    }

}