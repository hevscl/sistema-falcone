import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class TogglePasswordService {

    static emitirMostrarPassword = new EventEmitter<boolean>()

    static mostrar_password : boolean

    enviaMostrarPassword(mostrar_password :boolean){
        TogglePasswordService.mostrar_password = mostrar_password
        TogglePasswordService.emitirMostrarPassword.emit(mostrar_password)
    }

}