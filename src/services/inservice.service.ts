import { Injectable } from '@angular/core';
import { Http, ResponseOptions, RequestOptions, Headers, Response } from '@angular/http'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { map, throttle, tap, catchError } from 'rxjs/operators'
import { Router } from '@angular/router'
import { HttpErrorHandler, HandleError } from './http-handle-error.service';
import { Constants } from '../utils/Constantes'

@Injectable({
  providedIn: 'root'
})
export class InserviceService {

private handleError: HandleError;

authUrl = Constants.API_URL
apiUrlImg = Constants.API_IMAGENS

constructor(private http: Http, private router: Router, httpErrorHandler: HttpErrorHandler
) {
	this.handleError = httpErrorHandler.createHandleError('AuthService')
	}

  unidades(){
    let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}unidades`, {}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
				catchError(this.handleError('solicitarUnidades', null))
	 		)
	}
	
	buscaCep(cep){
		return this.http.get(`//viacep.com.br/ws/${cep}/json`)
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}

  buscarLojas(ndata1: string, ndata2: string, loja: number){
    let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}dashboard`, {'data_inicio': ndata1, 'data_fim': ndata2, 'id_unidade':loja}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}
	
	buscaListaUnidade(){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}unidades_cadastradas`, {}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}

	buscaListaOperador(idunidade){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}operador_unidade`, {'id_unidade': idunidade}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 			//	console.log(res)
	 			})
	 		)
	}
	
	buscauto(ndata1: string, ndata2: string, loja: number){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}dashboard`, {'data_inicio': ndata1, 'data_fim': ndata2, 'id_unidade':loja}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
					 //console.log(res)
					 catchError(this.handleError('solicitarDadosAutomaticos', null))
				 })
	 		)
	}

	gravaUnidade(codigo: string, nome_unidade: string, cep: string, bairro: string, valor_troco: string, cnpj: string, rua: string, cidade: string, numero: string, estado: string, img_base64: string){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}cadastra_unidade`, {'codigo_proposta': codigo, 'nome_fantasia': nome_unidade, 'cep': cep, 'bairro': bairro, 'limite_troco': valor_troco, 'cnpj': cnpj, 'endereco': rua, 'cidade': cidade, 'numero': numero, 'estado': estado, 'img_base64' : img_base64}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}

	editaUnidade(id: string, nome_unidade: string, cep: string, bairro: string, valor_troco: string, cnpj: string, rua: string, cidade: string, numero: string, estado: string, img :string){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}atualiza_unidade`, {'id_unidade': id, 'nome_fantasia': nome_unidade, 'cep': cep, 'bairro': bairro, 'limite_troco': valor_troco, 'cnpj': cnpj, 'endereco': rua, 'cidade': cidade, 'numero': numero, 'estado': estado, 'img' : img}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}

	enviaDadosExtrato(id_loja: number, id_operador: number, ndata1: any, ndata2: any){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}extrato_trocos`, {'id_unidade': id_loja, 'id_operador': id_operador, 'data_inicio': ndata1, 'data_fim': ndata2}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}

	enviaDadosExtratoFechamento(id_loja: number, id_operador: number, ndata1: any, ndata2: any){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}extrato_fechamento_caixa`, {'id_unidade': id_loja, 'id_operador': id_operador, 'data_inicio': ndata1, 'data_fim': ndata2}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}

	enviaDadosExtratoReserva(id_loja: number, ndata1: any, ndata2: any){
		let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}extrato_reserva`, {'id_unidade': id_loja, 'data_inicio': ndata1, 'data_fim': ndata2}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	}


  /*login(cnpj: string, senha: string): Observable<string>{
    console.log('cnpj '+ cnpj +' senha '+senha)
    return this.http.post(`${this.authUrl}/login_web`, {cnpj, senha})
      .pipe(
        map((res: Response) =>  res.json()),
          tap(res => {
              if(res.token){
                this.mostrarMenuE.emit(true)
                //this.mostrarloginE.emit(false)
                this.loggedIn = true
                this.router.navigate(['/deposito'])
              }else{
                this.mostrarMenuE.emit(false)
                //this.mostrarloginE.emit(true)
              }
          })
      )
  }
  
  verSaldo(){
	 	let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`);
	 	return this.http.get(`${this.authUrl}/saldo_web`, {headers})
	 		.pipe(
				 map((res: Response) => res.json()),
				 tap(res => {
				 	//console.log(res)
				 })
	 		 )
	 }

	 deposito(valenvio: number){
	 	let headers = new Headers()
	 	let token = localStorage.getItem('auth_token')
	 	headers.append('Authorization', `Bearer ${token}`)
	 	return this.http.post(`${this.authUrl}/gerafatura_web`, {'valor': valenvio}, {headers})
	 	.pipe(
	 			map((res: Response) => res.json()),
	 			tap(res => {
	 				//console.log(res)
	 			})
	 		)
	 }
  
  
  */
}
