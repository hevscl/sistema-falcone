import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatIconModule, MatCheckboxModule, MatRadioModule } from '@angular/material'
import { HttpErrorHandler } from '../services/http-handle-error.service';
import { MessageService }  from '../services/message.service';

import { MyDatePickerModule } from 'mydatepicker'
import { HttpModule } from '@angular/http'
import { DataTableModule } from 'angular-6-datatable';
import { DataTablesModule } from  'angular-datatables';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
registerLocaleData(ptBr)
import { CurrencyMaskModule } from 'ngx-currency-mask';
import { NgxMaskModule } from 'ngx-mask';
import { TextMaskModule } from 'angular2-text-mask'
import { NgxPaginationModule } from 'ngx-pagination'
import { UiSwitchModule } from 'ngx-toggle-switch';

import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ngx-currency-mask/src/currency-mask.config";

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

import { ImageCropperModule } from 'ng2-img-cropper';

import { AppRoutingModule } from '../rotas/app.routes';

import { Auth } from '../services/auth.service';
import { AuthGuard } from '../services/guard/auth-guard.service';
import { AuthGuardIn } from '../services/guard/auth-in-guard.service';
import { Clean } from '../utils/clean';

import { Requests } from '../services/requests.service';
import { LottieAnimationViewModule } from 'ng-lottie';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { EsconderCelular } from './../utils/esconderCelular.pipe';
import { CpfMaskPipe } from './../utils/cpfMaskPipe.pipe';
import { CnpjPipe } from './../utils/pipes/cnpj.pipe';
import { CelularPipe } from './../utils/pipes/celular.pipe';
import { TelefonePipe } from './../utils/pipes/telefone.pipe';
import { CepPipe } from './../utils/pipes/cep.pipe';

import { AppComponent } from './app.component';
import { AcessoComponent } from './acesso/acesso.component';
import { LoginComponent } from './acesso/login/login.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { LoaderComponent } from '../utils/loader/loader.component';
import { PagesComponent } from './pages/pages.component';
import { TopoComponent } from './pages/topo/topo.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AcessoPlComponent } from './acesso-pl/acesso-pl.component';
import { LoginPlComponent } from './acesso-pl/login-pl/login-pl.component';
import { BannerComponent } from './acesso-pl/banner/banner.component';
import { DepositoComponent } from './pages/deposito/deposito.component';
import { LojasComponent } from './pages/lojas/lojas.component';
import { OperadorComponent } from './pages/operador/operador.component';
import { FuncoesComponent } from './pages/funcoes/funcoes.component';
import { ExtratoComponent } from './pages/extrato/extrato.component';
import { GerenciaComponent } from './pages/gerencia/gerencia.component';
import { UnidadesComponent } from './pages/lojas/unidades/unidades.component';
import { AssinaturaComponent } from './pages/gerencia/assinatura/assinatura.component';
import { TransferenciaComponent } from './pages/gerencia/transferencia/transferencia.component';
import { MensalidadesComponent } from './pages/gerencia/assinatura/mensalidades/mensalidades.component';
import { TrocosPassadosComponent } from './pages/extrato/trocos-passados/trocos-passados.component';
import { FechamentoCaixaComponent } from './pages/extrato/fechamento-caixa/fechamento-caixa.component';
import { ReservasInseridasComponent } from './pages/extrato/reservas-inseridas/reservas-inseridas.component';
import { UnidadesmodalComponent } from './pages/lojas/unidadesmodal/unidadesmodal.component';
import { ColaboradoresComponent } from './pages/funcoes/colaboradores/colaboradores.component';
import { ErrorsComponent } from '../utils/errors/errors.component';
import { BancosComponent } from './pages/gerencia/transferencia/bancos/bancos.component';
import { GerenciarOperadorComponent } from './pages/operador/gerenciar-operador/gerenciar-operador.component';
import { EditarOperadorComponent } from './pages/operador/editar-operador/editar-operador.component';
import { EditarUnidadeComponent } from './pages/lojas/editar-unidade/editar-unidade.component';
import { ResponsesComponent } from '../utils/responses/responses.component';
import { MenuComponent } from './pages/menu/menu.component';
import { UltimoOperadorComponent } from './pages/operador/ultimo-operador/ultimo-operador.component';
import { UltimoDepositoComponent } from './pages/deposito/ultimo-deposito/ultimo-deposito.component';
import { AcessPagesComponent } from './layout/acess-pages/acess-pages.component';
import { OperadorModalComponent } from './pages/extrato/operadormodal/operadormodal.component';
import { EstornoComponent } from './pages/gerencia/estorno/estorno.component';

import { OperadorescadastradosComponent } from './pages/operador/operadorescadastrados/operadorescadastrados.component';
import { EditarImagemComponent } from './pages/gerencia/editar-imagem/editar-imagem.component';
import { EnviarImagemComponent } from './pages/lojas/enviar-imagem/enviar-imagem.component';
import { Constants } from 'src/utils/Constantes';
import { TogglePasswordComponent } from '../utils/toggle-password/toggle-password.component';
import { RecebiveisComponent } from './pages/recebiveis/recebiveis.component';
import { ExtratoRecebiveisComponent } from './pages/recebiveis/extrato-recebiveis/extrato-recebiveis.component';
import { ExtratoTransferenciasComponent } from './pages/recebiveis/extrato-transferencias/extrato-transferencias.component';
import { VendasComponent } from './pages/vendas/vendas.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { GerenciarProdutoComponent } from './pages/produtos/gerenciar-produto/gerenciar-produto.component';
import { ProfissionalSaudeComponent } from './pages/profissional-saude/profissional-saude.component';
import { GerenciarProfissionalComponent } from './pages/profissional-saude/gerenciar-profissional/gerenciar-profissional.component';
import { EditarProfComponent } from './pages/profissional-saude/editar-prof/editar-prof.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { GerenciarComponent } from './pages/clientes/gerenciar/gerenciar.component';
import { HistoricoComponent } from './pages/historico/historico.component';
import { IndicacaoComponent } from './pages/indicacao/indicacao.component';
import { HistoricoProfissionalComponent } from './pages/historico-profissional/historico-profissional.component';
import { FuncoesAdmComponent } from './pages/funcoes-adm/funcoes-adm.component';
import { EditarClienteComponent } from './pages/clientes/editar-cliente/editar-cliente.component';
import { EditarColaboradorComponent } from './pages/funcoes-adm/editar-colaborador/editar-colaborador.component';
import { ListaProdutoComponent } from './pages/produtos/lista-produto/lista-produto.component';
import { ListaClienteComponent } from './pages/clientes/lista-cliente/lista-cliente.component';
import { EditarProdutoComponent } from './pages/produtos/editar-produto/editar-produto.component';
import { ListaProfComponent } from './pages/profissional-saude/lista-prof/lista-prof.component';
import { MeusDadosComponent } from './pages/meus-dados/meus-dados.component';
import { MeusDadosColabComponent } from './pages/meus-dados-colab/meus-dados-colab.component';
import { EnviaImagemProfissionalComponent } from './pages/profissional-saude/envia-imagem-profissional/envia-imagem-profissional.component';
import { AcessoInternoComponent } from './acesso-interno/acesso-interno.component';
import { LoginInternoComponent } from './acesso-interno/login-interno/login-interno.component';
import { EnviaImagemProdutoComponent } from './pages/produtos/envia-imagem-produto/envia-imagem-produto.component';
import { ListarBuscaProfissionalComponent } from './pages/profissional-saude/gerenciar-profissional/listar-busca-profissional/listar-busca-profissional.component';
import { ReceitaComponent } from './pages/receita/receita.component';
import { ListarBuscaProdutosComponent } from './pages/produtos/gerenciar-produto/listar-busca-produtos/listar-busca-produtos.component';
import { MudarSenhaComponent } from './pages/meus-dados-colab/mudar-senha/mudar-senha.component';

@NgModule({
  declarations: [
    AppComponent,
    AcessoComponent,
    LoginComponent,
    CadastroComponent,
    LoaderComponent,
    PagesComponent,
    TopoComponent,
    DashboardComponent,
    LoginPlComponent,
    AcessoPlComponent,
    BannerComponent,
    DepositoComponent,
    LojasComponent,
    OperadorComponent,
    FuncoesComponent,
    ExtratoComponent,
    GerenciaComponent,
    UnidadesComponent,
    AssinaturaComponent,
    TransferenciaComponent,
    MensalidadesComponent,
    TrocosPassadosComponent,
    FechamentoCaixaComponent,
    ReservasInseridasComponent,
    UnidadesmodalComponent,
    ColaboradoresComponent,
    ErrorsComponent,
    BancosComponent,
    GerenciarOperadorComponent,
    EditarOperadorComponent,
    EditarUnidadeComponent,
    ResponsesComponent,
    MenuComponent,
    UltimoOperadorComponent,
    UltimoDepositoComponent,
    AcessPagesComponent,
    OperadorModalComponent,
    EstornoComponent,
    EditarColaboradorComponent,
    OperadorescadastradosComponent,
    EditarImagemComponent,
    EnviarImagemComponent,
    EsconderCelular,
    CpfMaskPipe,
    CnpjPipe,
    CelularPipe,
    TelefonePipe,
    CepPipe,
    TogglePasswordComponent,
    RecebiveisComponent,
    ExtratoRecebiveisComponent,
    ExtratoTransferenciasComponent,
    VendasComponent,
    ProdutosComponent,
    GerenciarProdutoComponent,
    ProfissionalSaudeComponent,
    GerenciarProfissionalComponent,
    EditarProfComponent,
    ClientesComponent,
    GerenciarComponent,
    HistoricoComponent,
    IndicacaoComponent,
    HistoricoProfissionalComponent,
    FuncoesAdmComponent,
    EditarClienteComponent,
    ListaProdutoComponent,
    ListaClienteComponent,
    EditarProdutoComponent,
    ListaProfComponent,
    MeusDadosComponent,
    MeusDadosColabComponent,
    EnviaImagemProfissionalComponent,
    AcessoInternoComponent,
    LoginInternoComponent,
    EnviaImagemProdutoComponent,
    ListarBuscaProfissionalComponent,
    ReceitaComponent,
    ListarBuscaProdutosComponent,
    MudarSenhaComponent
  ],
  imports: [
    BrowserModule,
    LottieAnimationViewModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    AppRoutingModule,
    SweetAlert2Module.forRoot(),
    NgxMaskModule.forRoot(),
    MyDatePickerModule,
    TextMaskModule,
    MatCheckboxModule,
    MatRadioModule,
    CurrencyMaskModule,
    NgxPaginationModule,
    FormsModule,
    HttpModule,
    DataTablesModule,
    DataTableModule,
    UiSwitchModule,
    ImageCropperModule
  ],
  providers: [ Auth, AuthGuard, AuthGuardIn,Requests, HttpErrorHandler, 
    MessageService, Clean, Constants,
    { provide: LOCALE_ID, useValue: 'pt-PT' }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
