import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ListaBuscaProfissionalService } from 'src/services/modules_services/lista_busca_profissional.service';

@Component({
  selector: 'app-gerenciar-profissional',
  templateUrl: './gerenciar-profissional.component.html',
  styleUrls: ['./gerenciar-profissional.component.css'],
  providers: [ ListaBuscaProfissionalService ]
})
export class GerenciarProfissionalComponent implements OnInit {

  public formBuscarProfissional : FormGroup = new FormGroup({
    'nome' : new FormControl(null)
  });

  public estadoLoad : boolean = false
  public mensagem_loader : string = ""

  public profissionais_validos : boolean = true
  public msg_profissionais_erro : string = ""

  public all_profissionais : boolean = true


  constructor(
    private requests : Requests,
    private listaBuscaProfissionalService : ListaBuscaProfissionalService) { }

  ngOnInit() {

  }

  buscarProfissionalViaInput(){
    if(this.formBuscarProfissional.value.nome.length>=3){
      this.buscarProfissional()
    }
  }

  buscarProfissional(){

      this.mostrarLoader(true, "Buscando Profissionais");

      let dados_profissionais : any = {
        'nome' : this.formBuscarProfissional.value.nome,
        'limit' : 20,
        'offset' : 0
      }
  
      this.requests.buscarProfissionais(dados_profissionais).subscribe(
        res => {
          this.mostrarLoader(false, null)
          var resp_busca : any = res
          if(resp_busca.code==200){
            this.mostrarProfissionais(true, null)
            this.listaBuscaProfissionalService.enviaProfissional(resp_busca.data);
          }else{
            this.mostrarProfissionais(false, resp_busca.mensagem)
          }
        }
      )
  }

  mostrarLoader(status: boolean, mensagem : string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarProfissionais(status: boolean, mensagem_erro : string){
    this.all_profissionais = false
    this.profissionais_validos = status
    this.msg_profissionais_erro = mensagem_erro
  }

}
