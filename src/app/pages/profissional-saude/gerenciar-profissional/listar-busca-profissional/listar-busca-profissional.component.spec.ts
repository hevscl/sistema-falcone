import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarBuscaProfissionalComponent } from './listar-busca-profissional.component';

describe('ListarBuscaProfissionalComponent', () => {
  let component: ListarBuscaProfissionalComponent;
  let fixture: ComponentFixture<ListarBuscaProfissionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarBuscaProfissionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarBuscaProfissionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
