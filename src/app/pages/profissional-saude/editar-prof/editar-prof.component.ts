import { Component, OnInit } from '@angular/core';
import { EditProfissionalService } from 'src/services/modules_services/edit_profissional.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Constants } from 'src/utils/Constantes';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { Requests } from 'src/services/requests.service';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
declare var $;

@Component({
  selector: 'app-editar-prof',
  templateUrl: './editar-prof.component.html',
  styleUrls: ['./editar-prof.component.css'],
  providers: [ EditProfissionalService,
    ValidaCpf,
    TelefoneValidation,
    ImagemModuleService,
    ResponsesModuleService ]
})
export class EditarProfComponent implements OnInit {
  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public estadoLoad : boolean = false
  public responseScreen : boolean = false
  public mensagem_loader : string = ""

  public marcas : any
  public erro_marca : String = null

  public profissional: any = EditProfissionalService.profissional
  public id_profissional: number

  public comissionado : boolean = false
  public status_comissionado : number = 0

  public text_button_img : string = "ADICIONAR FOTO"
  public img_profi : string = "/assets/new_user_img.svg"
  public img_base64 : string = null
  public url_img_profi : string = Constants.API_IMAGENS_PROFISSIONAIS

  public formEditarProfissional :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'endereco' : new FormControl(null),
    'codigo_profissional' : new FormControl(null),
    'email' : new FormControl(null),
    'telefone' : new FormControl(null),
    'data_nasc' : new FormControl(null),
    'senha' : new FormControl(null)
  })

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public enderecoValido : boolean = false
  public codValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public data_nascValido : boolean = false
  public senhaValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(private rotas: Router,
    private validaCpf : ValidaCpf,
    private requests : Requests,
    private validaTelef : TelefoneValidation,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_profi = imagem
        this.img_base64 = imagem
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          this.mostrarResponseScreen(false, null, null, null)
          //this.inputsCadastroUnidade = (this.keepCadastro) ? true : false
        }
      }
    )

    this.setInfo()
    this.listarMarcas()
  }

  setInfo() {
    if (this.profissional != undefined) {
      this.formEditarProfissional.get('nome').setValue(this.profissional.nome)
      this.formEditarProfissional.get('cpf').setValue(this.profissional.cpf)
      this.formEditarProfissional.get('endereco').setValue(this.profissional.endereco)
      this.formEditarProfissional.get('codigo_profissional').setValue(this.profissional.crm)
      this.formEditarProfissional.get('email').setValue(this.profissional.email)
      this.formEditarProfissional.get('telefone').setValue(this.profissional.celular)
      this.formEditarProfissional.get('data_nasc').setValue(this.profissional.data_nascimento)

      this.id_profissional = this.profissional.id
      this.status_comissionado = this.profissional.comissionado

      if(this.status_comissionado===1){
        this.comissionado = true
      }else{
        this.comissionado = false
      }

      
      this.validaNome("")
      this.validaCPF(this.formEditarProfissional.get('cpf'))
      this.validaEmail("")
      this.validaData("")
      this.validaTelefone(this.formEditarProfissional.get('telefone'))
      this.validaEndereco("")
      this.validaCodigo("")

      if(this.profissional.img!=null){
        this.img_profi = this.url_img_profi+this.profissional.img
        this.text_button_img = "ALTERAR FOTO"
      }

    } else {
      this.rotas.navigate(["/dashboard/gerenciar-profissional"])
    }
  }

  listarMarcas(){
    this.requests.listarMarcas().subscribe(
      res => {
        var resp_marcas : any = res
        if(resp_marcas.code==200){
          this.marcas = resp_marcas.data
          console.log("MARCAS :", this.marcas)
        }else{
          this.erro_marca = resp_marcas.mensagem
        }
        console.log(resp_marcas)
      }
    )
  }

  salvarAlteracoes(){
    this.mostrarLoader(true, "Atualizando Profissional")

    let dados_profissional : any = {
      'id' : this.id_profissional,
      'nome' : this.formEditarProfissional.value.nome,
      'cpf' : this.formEditarProfissional.value.cpf,
      'crm' : this.formEditarProfissional.value.codigo_profissional,
      'data_nasc' : this.formEditarProfissional.value.data_nasc,
      'endereco' : this.formEditarProfissional.value.endereco,
      'email' : this.formEditarProfissional.value.email,
      'celular' : this.formEditarProfissional.value.telefone,
      'img_base64' : this.img_base64,
      'comissionado' : this.status_comissionado
    }

    this.requests.atualizarProfissional(dados_profissional).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_update : any = res
        if(resp_update.code===200){
          this.profissional = resp_update.data[0]
          console.log(this.profissional)
          this.setInfo()
        }else{

        }
        this.mostrarResponseScreen(
          true,
          resp_update.code,
          resp_update.mensagem,
          "400px"
        )
      }
    )
  }

  validaNome(nome : string){
    if(this.formEditarProfissional.value.nome.length<7 || this.formEditarProfissional.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formEditarProfissional.value.cpf)
    this.ativarBotao()
  }


  validaEndereco(endereco : string){
    if(this.formEditarProfissional.value.endereco.length>5){
      this.enderecoValido = true
      
    }else{
      this.enderecoValido = false
    }
    this.ativarBotao()
  }

  validaCodigo(cod : string){
    if(this.formEditarProfissional.value.codigo_profissional.length>3){
      this.codValido = true
      
    }else{
      this.codValido = false
    }
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formEditarProfissional.value.email.indexOf('@') == -1 || this.formEditarProfissional.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(this.formEditarProfissional.value.telefone)
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formEditarProfissional.value.data_nasc.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  validaSenha(senha: string){
    if(this.formEditarProfissional.value.senha.length>7){
      this.senhaValido = true
    }else{
      this.senhaValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.telefoneValido && this.emailValido && 
      this.codValido && this.enderecoValido && this.data_nascValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  putProfissionalComissionado(){
    this.comissionado = !this.comissionado
    this.comissionado?this.status_comissionado = 1: this.status_comissionado = 0
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
