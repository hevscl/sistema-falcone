import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProfComponent } from './editar-prof.component';

describe('EditarProfComponent', () => {
  let component: EditarProfComponent;
  let fixture: ComponentFixture<EditarProfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
