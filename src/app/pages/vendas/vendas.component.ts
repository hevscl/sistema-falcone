import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Requests } from 'src/services/requests.service';
import { ListaProdutoService } from 'src/services/modules_services/lista_produtos.service';
import { Constants } from 'src/utils/Constantes';
import { Router } from '@angular/router';
import { ProdutosVendaService } from 'src/services/modules_services/produtos_vendidos.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';

@Component({
  selector: 'app-vendas',
  templateUrl: './vendas.component.html',
  styleUrls: ['./vendas.component.css'],
  providers: [ 
    ListaProdutoService,
    ResponsesModuleService,
    ProdutosVendaService ]
})
export class VendasComponent implements OnInit {
  public url_imagens : string = Constants.API_IMAGENS_PRODUTOS

  public formReceita :FormGroup = new FormGroup({
    'codigo' : new FormControl(null)
  })

  public formValorReceita :FormGroup = new FormGroup({
    'valor' : new FormControl(null)
  })

  public formProfissional :FormGroup = new FormGroup({
    'nome' : new FormControl(null)
  })

  public formCliente :FormGroup = new FormGroup({
    'nome_cliente' : new FormControl(null),
    'cpf_cliente' : new FormControl(null),
    'email_cliente' : new FormControl(null),
    'nasc_cliente' : new FormControl(null),
    'endereco_cliente' : new FormControl(null),
    'celular_cliente' : new FormControl(null)
  })

  public formBuscarProduto :FormGroup = new FormGroup({
    'nome' : new FormControl(null)
  })

  public dados_produtos : any
  public msg_no_produtos : string
  public msg_receita_erro : string

  public receita : any

  public outros_prod : any = null
  public status_outros_prod : boolean = false

  public produtos_selecionados = []

  public profissional = null
  public url_img_profi : string = Constants.API_IMAGENS_PROFISSIONAIS
  public img_profissional : string = "/assets/new_user_img.svg"

  public receita_valida : boolean = true
 
  public vendaPresents : boolean = true
  public estadoLoad : boolean = false
  public responseScreen : boolean = false
  public estadoProdutos : boolean = false
  public mensagem_loader : string

  public codigoValido : boolean = false
  public estadoForm : string = "disabled"
  public estadoForm2 : string = "disabled"

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  constructor( private requests : Requests,
    private responsesModuleService : ResponsesModuleService,
    private listaProdutosService : ListaProdutoService,
    private rotas : Router) { }

  ngOnInit() {
    this.redirecionarProfissional()

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
          this.mostrarResponseScreen(false, null, null, null)
          if(this.receita_valida===false){
            this.vendaPresents = true
          }
      }
    )

    ProdutosVendaService.emitirProdutosVenda.subscribe(
      dados_produtos => {
        this.adicionaOuRemove(dados_produtos.status, dados_produtos.produto)
      }
    )
  }

  redirecionarProfissional(){
    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    if(dados_conta.funcionalidades[0].tipo_usuario===2){
      this.rotas.navigate(['/dashboard/indicacao']);
    }
  }

  resetDados(){
    this.formBuscarProduto.reset()
    this.formReceita.reset()
    this.codigoValido = false
    this.formCliente.reset()
    this.formProfissional.reset()
    this.formValorReceita.get('valor').setValue(0.00);
    this.profissional = null
    this.produtos_selecionados = []
    this.receita_valida = false
  }

  buscarReceita(){
    this.estadoProdutos = false
    this.mostrarLoader(true, "Buscando Receita")
    var dados_receita : any = {
      'codigo' : this.formReceita.value.codigo
    }

    this.requests.getReceita(dados_receita).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_receita : any = res
        this.vendaPresents = false
        if(resp_receita.code===200){
          this.mostrarReceita(true, "")
          this.mostrarProdutos(true, "")
          this.dados_produtos = resp_receita.produtos
          this.setDadosReceita(resp_receita.profissional[0], resp_receita.cliente[0])
          this.listaProdutosService.enviaProduto(this.dados_produtos)
          this.criaProdutosSelecionadosInicial(this.dados_produtos)
          this.listaProdutosService.enviaReceita(resp_receita.info[0])
          this.receita = resp_receita.info[0]
        }else{
          this.receita = null
          this.mostrarReceita(false, resp_receita.mensagem)
          //this.mostrarProdutos(false, resp_receita.mensagem);
        }
      }
    )
  }

  buscarProdutoViaInput(){
    if(this.formBuscarProduto.value.nome.length>=3){
      this.buscarProduto()
    }else{
      this.outros_prod = null
    }
  }

  buscarProduto(){

      //this.mostrarLoader(true, "Buscando Produtos");

      let dados_produtos : any = {
        'nome' : this.formBuscarProduto.value.nome,
        'categoria' : 0,
        'marca' : 0,
        'limit' : 20,
        'offset' : 0
      }
  
      this.requests.buscarProdutos(dados_produtos).subscribe(
        res => {
          //this.mostrarLoader(false, null)
          var resp_busca : any = res
          console.log(resp_busca)
          if(resp_busca.code==200){
            this.outros_prod = resp_busca.data
            //.mostrarProdutos(true, null)
            //this.listaProdutoService.enviaProduto(resp_busca.data);
          }else{
            this.outros_prod = null
            //this.mostrarProdutos(false, resp_busca.mensagem)
          }
        }
      )
  }

  showDescricao(descricao){
    if(descricao!=null){
      return descricao.substr(0,90) + " ..."
    }else{
      return "Produto sem descrição."
    }
    
  }

  finalizarReceita(){
    this.mostrarLoader(true, "Finalizando receita")
    let dados_receita : any = {
      'id_receita' : this.receita.id,
      'produtos' : this.produtos_selecionados,
      'valor_total' : this.formValorReceita.value.valor
    }
    console.log("Dados Finalizar:", dados_receita)

    this.requests.finalizarReceita(dados_receita).subscribe(
      res =>{
        this.mostrarLoader(false, null)
        var resp_receita : any = res
        console.log(resp_receita)
        if(resp_receita.code===200){
          this.resetDados()
        }
        this.mostrarResponseScreen(
          true,
          resp_receita.code,
          resp_receita.mensagem,
          "400px"
        )
      }
    )
  }

  setDadosReceita(profissional, cliente){
    if(profissional.img){
      this.img_profissional = this.url_img_profi+profissional.img
    }

    this.profissional = profissional;
    this.formProfissional.get('nome').setValue(profissional.nome);

    this.formCliente.get('nome_cliente').setValue(cliente.nome);
    this.formCliente.get('cpf_cliente').setValue(cliente.cpf);
    this.formCliente.get('email_cliente').setValue(cliente.email);
    this.formCliente.get('endereco_cliente').setValue(cliente.endereco);
    this.formCliente.get('celular_cliente').setValue(cliente.celular);

    this.formValorReceita.get('valor').setValue(0.00);
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  validaCodigo(codigo : string){
    if(this.formReceita.value.codigo.length === 6){
      this.codigoValido = true
      
    }else{
      this.codigoValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.codigoValido != false){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  ativarBotao2(){
    if(this.codigoValido != false && this.produtos_selecionados.length>0){
      this.estadoForm2 = ''
    }else{
      this.estadoForm2 = 'disabled'
    }
  }

  criaProdutosSelecionadosInicial(produtos){
    for(var i=0; i < produtos.length; i++){
      let index = this.produtos_selecionados.findIndex(produto_i => produto_i.id == produtos[i].id);
      if(index < 0 && produtos[i].status === 1) {
        this.produtos_selecionados.push({id: produtos[i].id, nome: produtos[i].nome, status : produtos[i].status});
      }
    }
    this.ativarBotao2()
    //console.log(this.produtos_selecionados)
  }

  mudarStatusProdutoSelecionado(evento, produto){
    this.status_outros_prod = !this.status_outros_prod
    this.adicionaOuRemove(this.status_outros_prod, produto)
    this.ativarBotao2()
  }

  adicionaOuRemove(status, produto) {
    let index = this.produtos_selecionados.findIndex(produto_s => produto_s.id == produto.id);
    if(index < 0 && status) {
          this.produtos_selecionados.push({id: produto.id, nome: produto.nome, status: produto.status});
    } else {
      if(status===false){
        this.produtos_selecionados.splice(index, 1);
      }
    }
    this.ativarBotao2()
    console.log(this.produtos_selecionados)
  }

  mostrarReceita(status: boolean, mensagem_erro: string){
    this.receita_valida = status
    this.msg_receita_erro = mensagem_erro
  }

  mostrarProdutos(status : boolean, mensagem_erro: string){
    this.estadoProdutos = status
    this.msg_no_produtos = mensagem_erro
  }

  mostrarImg(img : String){
    if(img!=null){
      return this.url_imagens+img
    }else{
      return "/assets/new_product_img.svg"
    }
  }

}
