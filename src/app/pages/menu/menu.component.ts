import { Component, OnInit } from '@angular/core';
import { Auth } from 'src/services/auth.service';
import { Router } from '@angular/router';
import { TopoMenuModuleService } from 'src/services/modules_services/topo_menu.module.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [TopoMenuModuleService],
  animations:[
    trigger('nomemenu', [
      state('hide', style({
        opacity: 0
      })),
      state('show', style({
        opacity: 1
      })),
      transition('hide => show', animate('1s ease-in')),
      transition('show => hide', animate('0.2s ease-in'))
    ])
  ]
})
export class MenuComponent implements OnInit {

  estadoLoad : boolean
  resposta_logout :any = []

  colaborador :any
  nome_colaborador :string
  cpf_colaborador :number
  funcionalidade :any
  is_admin :number
  tipo_conta :number

  funcionalidades :any

  
  constructor(private auth: Auth, private rotas: Router) { }

  //ativo: boolean = false

  ngOnInit() {

    TopoMenuModuleService.emitirAtivacao.subscribe(
      ativo => this.ativo = ativo
    )

    TopoMenuModuleService.emitirEstado.subscribe(
      estado => this.estado = estado
    )

    TopoMenuModuleService.emitirEstado1.subscribe(
      estado1 => this.estado1 = estado1
    )
    
    this.dadosDaConta()
    this.funcionalidadesDoColaborador()

  	//console.log(this.data.changeStg)
  }

  ativo: boolean = false
  public estado: string = 'show'
  public estado1: string = 'hide'

  dadosDaConta(){

    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    this.colaborador = dados_conta.usuario
    this.funcionalidade = dados_conta.funcionalidades

    this.cpf_colaborador = this.colaborador[0].cpf
    this.nome_colaborador = this.colaborador[0].nome
    this.is_admin = this.colaborador[0].admin
    this.tipo_conta = this.funcionalidade[0].tipo_usuario

  }

  funcionalidadesDoColaborador(){
    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    this.funcionalidades = dados_conta.funcionalidades
    console.log("Funcionalidade",this.funcionalidades)
  }

  logout(){
    this.loader(true)
    this.auth.logoutSisWeb()
    .subscribe(
      data => {
      this.loader(false)
      if(data != null){
        this.resposta_logout = data
        if(this.resposta_logout.code === 200){
          this.apagarDadosViaLogout();
        }else{
          this.apagarDadosViaLogout();
        } 
      }else{
        this.apagarDadosViaLogout();
      }
   })
  }

  apagarDadosViaLogout() :void{
    localStorage.removeItem('auth_token');
    localStorage.removeItem('dados_conta');
    if(this.tipo_conta!=null){
      if(this.tipo_conta === 1){
        this.rotas.navigate(['/adm']);
      }else{
        this.rotas.navigate(['/']);
      }
    }else{
      this.rotas.navigate(['/']);
    }
  }

  loader(status:boolean){
    if(status===true){
      $('#loadLogout').modal('show');
    }else{
      $('#loadLogout').modal('hide');
    }
  }

}
