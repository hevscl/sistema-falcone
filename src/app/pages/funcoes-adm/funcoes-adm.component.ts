import { FormGroup, FormControl } from '@angular/forms';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { Component, OnInit } from '@angular/core';
import { TogglePasswordService } from 'src/services/modules_services/toggle_password.service';
import { Requests } from 'src/services/requests.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';

@Component({
  selector: 'app-funcoes-adm',
  templateUrl: './funcoes-adm.component.html',
  styleUrls: ['./funcoes-adm.component.css'],
  providers: [ 
    TogglePasswordService,
    ResponsesModuleService, 
    ValidaCpf]
})
export class FuncoesAdmComponent implements OnInit {
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]

  public formColab :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'email' : new FormControl(null),
    'confirma_email' : new FormControl(null),
    'senha' : new FormControl(null)
  })

  public responseScreen : boolean = false
  public estadoLoad : boolean = false

  public mensagem_loader : string = ""

  public show_password : boolean = false

  public togglePassword : boolean = false

  public colaborador_admin : number = 0
  public admin : boolean = false

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public emailValido : boolean = false
  public conf_emailValido : boolean = false
  public senhaValida : boolean = false

  public estadoForm : string = "disabled"

  constructor(
    private validaCpf : ValidaCpf,
    private requests : Requests,
    private togglePasswordService : TogglePasswordService,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
      }
    )
  }

  adicionarColaborador(){
    this.mostrarLoader(true, "Adicionando Colaborador")
    let dados_colaborador : any = {
      'nome' : this.formColab.value.nome,
      'cpf' : this.formColab.value.cpf,
      'email' : this.formColab.value.email,
      'senha' : this.formColab.value.senha,
      'admin' : this.colaborador_admin
    }

    console.log(dados_colaborador)

    this.requests.insertColaborador(dados_colaborador).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_colab : any = res
        if(resp_colab.code===200){
          this.formColab.reset()
        }
        this.mostrarResponseScreen(
          true,
          resp_colab.code,
          resp_colab.mensagem,
          "400px"
        )
      }
    )
  }

  validaNome(nome : string){
    if(this.formColab.value.nome.length<7 || this.formColab.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formColab.value.cpf)
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formColab.value.email.indexOf('@') == -1 || this.formColab.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.validaConf_Email("")
    this.ativarBotao()
  }

  validaConf_Email(email_teste: string){
    if(this.formColab.value.confirma_email!=undefined){
      if(this.formColab.value.confirma_email.indexOf('@') == -1 || this.formColab.value.confirma_email.indexOf('.') == -1
    || this.formColab.value.confirma_email != this.formColab.value.email){
      this.conf_emailValido = false
    }else{
      this.conf_emailValido = true
    }
    this.ativarBotao()
    }
  }

  validaSenha(){
    if(this.formColab.value.senha.length>=5){
      this.senhaValida = true
    }else{
      this.senhaValida = false
    }
    this.mostrarTogglePassword()
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.emailValido && this.conf_emailValido && this.senhaValida){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  mostrarPassword(){
    this.show_password = !this.show_password
    if(this.show_password){
      this.togglePasswordService.enviaMostrarPassword(true)
    }else{
      this.togglePasswordService.enviaMostrarPassword(false)
    }
  }

  mostrarTogglePassword(){
    if(this.formColab.value.senha.length>0){
      this.togglePassword = true
    }else{
      this.togglePassword = false
    }
  }

  putColaboradorAdm(){
    this.admin = !this.admin;
    (this.admin)?this.colaborador_admin = 1:this.colaborador_admin =0
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
