import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { Requests } from 'src/services/requests.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
import { Constants } from 'src/utils/Constantes';
declare var $;

@Component({
  selector: 'app-meus-dados',
  templateUrl: './meus-dados.component.html',
  styleUrls: ['./meus-dados.component.css'],
  providers: [ 
    ValidaCpf,
    TelefoneValidation,
    ResponsesModuleService,
    ImagemModuleService ]
})
export class MeusDadosComponent implements OnInit {
  public responseScreen : boolean = false
  public estadoLoad : boolean = false

  public mensagem_loader : string = ""

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public formMeusDados :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'endereco' : new FormControl(null),
    'codigo_profissional' : new FormControl(null),
    'email' : new FormControl(null),
    'telefone' : new FormControl(null),
    'data_nasc' : new FormControl(null)
  });

  public togglePassword : boolean = false
  public toggleNewPassword : boolean = false
  public toggleNewRePassword : boolean = false

  public url_imagens_profi = Constants.API_IMAGENS_PROFISSIONAIS
  public img_profi : string = "/assets/new_user_img.svg"
  public img_base64 : string = null

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public enderecoValido : boolean = false
  public codValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public data_nascValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_profi = imagem
        this.img_base64 = imagem
        this.ativarBotao()
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
        }
      }
    )

    this.setInfo()
  }

  setInfo() {
    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    var colaborador = dados_conta.usuario

    this.mostrarImg(colaborador[0].img)
    this.formMeusDados.get('nome').setValue(colaborador[0].nome)
    this.formMeusDados.get('cpf').setValue(colaborador[0].cpf)
    this.formMeusDados.get('email').setValue(colaborador[0].email)
    this.formMeusDados.get('endereco').setValue(colaborador[0].endereco)
    this.formMeusDados.get('telefone').setValue(colaborador[0].celular)
    this.formMeusDados.get('codigo_profissional').setValue(colaborador[0].crm)
    this.formMeusDados.get('data_nasc').setValue(colaborador[0].data_nascimento)
    
    this.validaNome("")
    this.validaCPF(this.formMeusDados.get('cpf'))
    this.validaEmail("")
    this.validaEndereco("")
    this.validaTelefone("")
    this.validaCodigo("")
    this.validaData("")

    this.estadoForm = "disabled"
  }

  alterarDados(){
    this.mostrarLoader(true, "Atualizando Dados")
    let dados_pessoais: any = {
      'nome' : this.formMeusDados.value.nome,
      'crm' : this.formMeusDados.value.codigo_profissional,
      'email' : this.formMeusDados.value.email,
      'celular' : this.formMeusDados.value.telefone,
      'data_nasc' : this.formMeusDados.value.data_nasc,
      'endereco' : this.formMeusDados.value.endereco,
      'img_base64' : this.img_base64
    }

    this.requests.atualizarDadosPessoaisProfissional(dados_pessoais).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_dados : any = res
        if(resp_dados.code===200){
          this.putInStorage(resp_dados.data);
          this.setInfo();
        }
        this.mostrarResponseScreen(
          true,
          resp_dados.code,
          resp_dados.mensagem,
          "400px"
        )
      }
    )
  }

  putInStorage(dados_conta){
    localStorage.setItem('dados_conta', JSON.stringify(dados_conta))
  }

  validaNome(nome : string){
    if(this.formMeusDados.value.nome.length<7 || this.formMeusDados.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formMeusDados.value.cpf)
    this.ativarBotao()
  }


  validaEndereco(endereco : string){
    if(this.formMeusDados.value.endereco.length>5){
      this.enderecoValido = true
      
    }else{
      this.enderecoValido = false
    }
    this.ativarBotao()
  }

  validaCodigo(cod : string){
    if(this.formMeusDados.value.codigo_profissional.length>=4){
      this.codValido = true
      
    }else{
      this.codValido = false
    }
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formMeusDados.value.email.indexOf('@') == -1 || this.formMeusDados.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(this.formMeusDados.value.telefone)
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formMeusDados.value.data_nasc.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.telefoneValido && this.emailValido && 
      this.codValido && this.enderecoValido && this.data_nascValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  mostrarImg(img : String){
    if(img!=null){
      this.img_profi = this.url_imagens_profi+img
    }else{
      this.img_profi = "/assets/new_user_img.svg"
    }
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }


}
