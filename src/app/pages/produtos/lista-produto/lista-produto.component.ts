import { Component, OnInit } from '@angular/core';
import { ListaProdutoService } from 'src/services/modules_services/lista_produtos.service';
import { Constants } from '../../../../utils/Constantes'
import { Requests } from 'src/services/requests.service';
import { ProdutosVendaService } from 'src/services/modules_services/produtos_vendidos.service';

@Component({
  selector: 'app-lista-produto',
  templateUrl: './lista-produto.component.html',
  styleUrls: ['./lista-produto.component.css'],
  providers: [ 
    ListaProdutoService,
    ProdutosVendaService ]
})
export class ListaProdutoComponent implements OnInit {

  public dados_produtos : any = ListaProdutoService.produtos
  public receita : any = ListaProdutoService.receita

  public url_imagens : string = Constants.API_IMAGENS_PRODUTOS

  constructor(
    private requests : Requests,
    private produtosVendaService : ProdutosVendaService) { }

  ngOnInit() {
    
  }

  mudarStatusProduto(evento, id_produto){
    console.log(evento)
    var status = evento===true?1:0

    var dados_produto = {
      'id_receita' : this.receita.id,
      'id_produto' : id_produto,
      'status': status
    }

    this.requests.updateStatusProdutoReceita(dados_produto).subscribe(
      res => {
        var resp_status : any = res
        if(resp_status.code===200){
          this.dados_produtos = resp_status.data
        }
      }
    )
  }

  mudarStatusProdutoSelecionado(evento, produto){
    this.mudarStatusProduto(evento, produto.id)
    this.produtosVendaService.enviaProdutosVenda({status : evento, produto : produto})
  }

  mostrarImg(img : String){
    if(img!=null){
      return this.url_imagens+img
    }else{
      return "/assets/new_product_img.svg"
    }
  }

  showDescricao(descricao){
    if(descricao!=null){
      return descricao.substr(0,90) + " ..."
    }else{
      return "Produto sem descrição."
    }
  }

}
