import { CropperSettings } from 'ng2-img-cropper';
import { Component, OnInit } from '@angular/core';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { Requests } from 'src/services/requests.service';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';

@Component({
  selector: 'app-enviar-imagem',
  templateUrl: './enviar-imagem.component.html',
  styleUrls: ['./enviar-imagem.component.css'],
  providers: [ ImagemModuleService ]
})
export class EnviarImagemComponent implements OnInit {

  corteScreen : string = "p1"

  data: any;
  cropperSettings: CropperSettings;

  estadoLoad : boolean = false;
  mensagem_loader : string = "Carregando Dados"

  responseScreen : boolean = false

  constructor(
    public responsesModuleService : ResponsesModuleService,
    private requests : Requests,
    private imagemModuleService : ImagemModuleService
    ) {
      this.cropperSettings = new CropperSettings();
      this.cropperSettings.width = 500;
      this.cropperSettings.height = 500;
      this.cropperSettings.croppedWidth = 500;
      this.cropperSettings.croppedHeight = 500;
      this.cropperSettings.canvasWidth = 400;
      this.cropperSettings.canvasHeight = 300;

      this.data = {};
    }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        this.corteScreen = "p1"
        this.mostrarResponseScreen(false, null, null, null)
        this.data = {}
      }
    )
  }

  finishEdition(){
    if(this.data.image){
      this.corteScreen = "p2"
    }
  }

  confirmarFoto(){
    var img_negocio = this.data.image
    this.imagemModuleService.enviaImagemBase64(img_negocio)
    this.responsesModuleService.fecharResponse(true)
  }
  
  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }
}
