import { ValidaCnpj } from './../../../../utils/valida-cnpj';
import { EditUnidadeService } from './../../../../services/modules_services/edit_unidade.service';
import { InserviceService } from 'src/services/inservice.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorInputService } from 'src/services/modules_services/error_input.service';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { Requests } from 'src/services/requests.service';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-unidade',
  templateUrl: './editar-unidade.component.html',
  styleUrls: ['./editar-unidade.component.css'],
  providers: [ 
    AcessPagesService,
    EditUnidadeService,
    ErrorInputService,
    ResponsesModuleService,
    ErrorInputService,
    ValidaCnpj,
    ImagemModuleService  ]
})
export class EditarUnidadeComponent implements OnInit {

  public estadoForm2 : string = "disabled"

  public estadoLoad : boolean = false
  public responseScreen :boolean = false
  public mensagem_loader : string
  
  public cepValido : boolean = false
 
  public valor_trocoValido : boolean = false
  
  public nome_unidadeValido : boolean = false
  public bairroValido : boolean = false
  public cnpjValido : boolean = false
  public ruaValido : boolean = false
  public cidadeValido : boolean = false
  public numeroValido : boolean = false
  public estadoValido : boolean = false

  public errors_input : boolean = false

  public dados : any = EditUnidadeService.info_unidades

  public estadoAcess : boolean = false

  public cep = [/\d/,/\d/,/\d/,/\d/,/\d/, '-', /\d/, /\d/, /\d/]
  public cnpj = [/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/, '/', /\d/, /\d/, /\d/, /\d/,'-',/\d/,/\d/,]

  public formLojas :FormGroup = this.formBuilder.group({
    'id' : ['', []],
    'nome_unidade' : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    'cep' : ['', [Validators.required, Validators.minLength(8), Validators.maxLength(10)]],
    'bairro' : ['', Validators.required],
    'valor_troco' : ['', [Validators.required]],
    'cnpj' : ['', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]],
    'rua' : ['',[ Validators.required]],
    'cidade' : ['', [Validators.required]],
    'numero' : ['', [Validators.required]],
    'estado' : ['', [Validators.required]]
  })

  public foto_aprov : boolean = false;

  public img_matriz : string = "/assets/new_user_img.svg"
  public img_base64 : string = ""


  constructor(private acessPagesService : AcessPagesService, 
    private formBuilder: FormBuilder,
    private serv: InserviceService,
    private requests : Requests,
    private validaCnpj : ValidaCnpj,
    private responsesModuleService : ResponsesModuleService,
    private errorMensagemService : ErrorInputService,
    private router : Router) { }

  ngOnInit() {

    if(this.dados===undefined){
      this.router.navigate(['/dashboard/lojas'])
    }

    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => this.mostrarResponseScreen(false, null, null, null)
    )

    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_matriz = imagem
        this.img_base64 = imagem
      }
    )
    
    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Unidade de loja",
        style: "link_blur",
        router: "lojas"
      },
      {
        page: "Editar unidade de loja",
        style: "link_ativo",
        router: "atualizar-unidade"
      }]
    )

    ErrorInputService.emitirMensagemErro.subscribe(
      mensagem_error => {
        this.mostrarErrordeInput(true)
      }
    )

    this.setInfo()

  }

  setInfo(){
    if(this.dados!=undefined && this.dados.imagem!=(""||null|| "")){
      this.img_matriz = this.requests.apiUrlImg+this.dados.imagem
    }

    if(this.dados!=undefined && this.dados.status_aprov != null){
      if(this.dados.status_aprov===0 && this.dados.tipo_aprov==="imagem"){
        this.foto_aprov = true
      }
    }

    this.img_base64 = ""

    if(this.dados!=undefined){
      this.formLojas.get('id').setValue(this.dados.id)
      this.formLojas.get('nome_unidade').setValue(this.dados.nome_fantasia)
      this.formLojas.get('cnpj').setValue(this.dados.cnpj)
      this.formLojas.get('cep').setValue(this.dados.cep)
      this.formLojas.get('rua').setValue(this.dados.endereco)
      this.formLojas.get('numero').setValue(this.dados.numero)
      this.formLojas.get('bairro').setValue(this.dados.bairro)
      this.formLojas.get('cidade').setValue(this.dados.cidade)
      this.formLojas.get('estado').setValue(this.dados.estado)
      this.formLojas.get('valor_troco').setValue(this.dados.limite_troco)
    }

    this.validaCNPJ();
    this.validaNome(this.formLojas.get('nome_unidade'))
    this.validaCep(this.formLojas.get('cep'))
    this.validaBairro(this.formLojas.get('bairro'))
    this.validaRua(this.formLojas.get('rua'))
    this.validaEstado(this.formLojas.get('estado'))
    this.validaValorTroco()
    this.validaCidade(this.formLojas.get('cidade'))
    this.validaNumero(this.formLojas.get('numero'))
  }

  consultaCep(){
    let cep = this.formLojas.get('cep').value
    let novocep = cep.replace('-','')
    if(cep != ''){
      var validacep = /^[0-9]{8}$/;
      if(validacep.test(novocep)){

        this.serv.buscaCep(novocep).subscribe(dados => {
          
          //console.log(dados)
          this.formLojas.get('bairro').setValue(dados.bairro)
          this.formLojas.get('rua').setValue(dados.logradouro)
          this.formLojas.get('estado').setValue(dados.uf)
          this.formLojas.get('cidade').setValue(dados.localidade)
        })

      }
    }
  }

  atualizar(){

    this.validaValorTroco()
    if(this.valor_trocoValido){
      this.mostrarErrordeInput(false)
      this.mostrarLoader(true, "Atualizando unidade")
      this.serv.editaUnidade(
      this.formLojas.value.id,
      this.formLojas.value.nome_unidade, 
      this.formLojas.value.cep, 
      this.formLojas.value.bairro, 
      this.formLojas.value.valor_troco, 
      this.formLojas.value.cnpj, 
      this.formLojas.value.rua, 
      this.formLojas.value.cidade,
      this.formLojas.value.numero,
      this.formLojas.value.estado,
      this.img_base64).subscribe(
        dados => {

          console.log(this.img_base64)
          this.mostrarLoader(false, null)
          let code = dados.code
          this.dados = dados.data
          if(code == 200){       
            this.formLojas.reset();
            this.setInfo()
          }

          this.mostrarResponseScreen(
            true,
            code,
            dados.mensagem,
            "300px"
          )
        }
      )
    }else{
      this.errorMensagemService.enviaMensagemErro("O valor de limite de troco deve ser maior ou igual a R$ 20,00")
    }
    
  }

  getImgMatriz(){
    this.requests.solicitarImgMatriz()
    .subscribe(
      res => {
        var resp_img : any = res
        if(resp_img.code === 200){
          this.img_matriz = this.requests.apiUrlImg+resp_img.data
        }
        this.img_base64 = ""
      }
    )
  }

  validaCNPJ(){
    this.cnpjValido = this.validaCnpj.validarCNPJ(this.formLojas.value.cnpj)
    this.ativarBotao()
  }

  validaNome(nome){
    if(this.formLojas.value.nome_unidade.length>5){
      this.nome_unidadeValido = true   
    }else{
      this.nome_unidadeValido = false
    }
    this.ativarBotao()
  }

  validaBairro(bairro){
    if(this.formLojas.value.bairro.length>=2){
      this.bairroValido = true
    }else{
      this.bairroValido = false
    }
    this.ativarBotao()
  }

  validaRua(rua){
    if(this.formLojas.value.rua.length>1){
      this.ruaValido = true
      
    }else{
      this.ruaValido = false
    }
    this.ativarBotao()
  }

  validaCidade(cidade){
    if(this.formLojas.value.cidade.length>2){
      this.cidadeValido = true
      
    }else{
      this.cidadeValido = false
    }
    this.ativarBotao()
  }

  validaEstado(estado){
    if(this.formLojas.value.estado.length == 2){
      this.estadoValido = true
      
    }else{
      this.estadoValido = false
    }
    this.ativarBotao()
  }

  validaCep(cep){
    var cep = this.formLojas.value.cep.replace("-","")
    var cepFinal = cep.replace(/_/g,"")
      if(cepFinal.toString().length == 8){
        this.cepValido = true
      }else{
        this.cepValido = false
      }
      this.ativarBotao()
  }

  validaValorTroco(){
    if(parseFloat(this.formLojas.value.valor_troco)>=20){
      this.valor_trocoValido = true
    }else{
      this.valor_trocoValido = false
    }
    this.ativarBotao()
  }

  validaNumero(numero){
    if(this.formLojas.value.numero>0){
      this.numeroValido = true
    }else{
      this.numeroValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.cnpjValido && this.nome_unidadeValido == true && this.cepValido == true && this.ruaValido== true 
    && this.bairroValido == true && this.estadoValido == true 
    && this.cidadeValido == true && this.numeroValido == true){
      this.estadoForm2 = ''
    }else{
      this.estadoForm2 = 'disabled'
    }
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarErrordeInput(status :boolean){
    this.errors_input = status
  }

}
