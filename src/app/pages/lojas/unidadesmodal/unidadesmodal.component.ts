import { Validators } from '@angular/forms';
import { InserviceService } from './../../../../services/inservice.service';
import { Component, OnInit } from '@angular/core';
import { Unidades } from '../../../../models/unidade.model';
import { UnidadeSelected } from '../../../../models/unidade_selected.model';
import { Requests } from '../../../../services/requests.service';
import { UnidadeSelectedService } from '../../../../services/unidade_selected.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
declare var $

//flag pra verificar o primeiro load da lista de unidades
var flag = 1;

@Component({
  selector: 'app-unidadesmodal',
  templateUrl: './unidadesmodal.component.html',
  styleUrls: ['./unidadesmodal.component.css'],
  providers: [ UnidadeSelectedService ]
})



export class UnidadesmodalComponent implements OnInit {
  public data : any
  unidades: Unidades
  unidade_selected: UnidadeSelected
  resposta_unidades :any = []

  public p
  dtElement: DataTableDirective;
  ativo: boolean = false
  alldados: any = []
  dtOptions: DataTables.Settings = {}
  dtTrigger: Subject<any> = new Subject()

  estadoLoad: boolean = true

  unidade_selecionada :number = 0

  constructor(private request :Requests, private unidadeSelectedService :UnidadeSelectedService,
    private serv: InserviceService) { }

  ngOnInit() {
    this.fecharLoad(true)
    this.request.getUnidades()
    .subscribe(
      data => {
        this.fecharLoad(false)
        this.resposta_unidades = data
        if(this.resposta_unidades.code === 200){
          this.unidades = this.resposta_unidades.data
        }else{
          
        } 
    })
    
  }

  mudarParametro(id, nome_fantasia, endereco, numero, bairro, cidade, saldo){
    this.unidade_selected = { 
      id, nome_fantasia, endereco, numero, bairro, cidade, saldo 
    }

    this.unidadeSelectedService.enviaUnidadeSelecionada(this.unidade_selected)
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }

 
}
