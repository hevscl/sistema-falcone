import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidadesmodalComponent } from './unidadesmodal.component';

describe('UnidadesmodalComponent', () => {
  let component: UnidadesmodalComponent;
  let fixture: ComponentFixture<UnidadesmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnidadesmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnidadesmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
