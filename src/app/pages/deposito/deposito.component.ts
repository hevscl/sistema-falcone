import { Component, OnInit } from '@angular/core';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Requests } from 'src/services/requests.service';
import { ErrorInputService } from 'src/services/modules_services/error_input.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { UltimoDepositoModuleService } from 'src/services/modules_services/ultimo_deposito_module.service';

@Component({
  selector: 'app-deposito',
  templateUrl: './deposito.component.html',
  styleUrls: ['./deposito.component.css'],
  providers: [ 
    UnidadeSelectedService, 
    ErrorInputService, 
    ResponsesModuleService,
    UltimoDepositoModuleService
  ]
})
export class DepositoComponent implements OnInit {

  public titulo_pagina : string = "Escolha a unidade para fazer depósito"
  
  public estadoLoad : boolean = false
  public responseScreen :boolean = false
  public mensagem_loader : string

  public unidade_selecionada : any

  public valor_deposito : string
  private url_fatura : string = null

  public errors_input : boolean = false

  public formDeposito : FormGroup = new FormGroup({
    'valor_deposito' : new FormControl(null)
  })

  constructor(private requests : Requests,
    private errorMensagemService : ErrorInputService,
    private responsesModuleService : ResponsesModuleService,
    private ultimoDepositoModuleResponse : UltimoDepositoModuleService ) { }

  ngOnInit() {

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      UnidadeSelected => { 
        this.unidade_selecionada = UnidadeSelected 
        this.mostrarErrordeInput(false)
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        this.mostrarResponseScreen(false, null, null, null)
        if(this.url_fatura!=null){
          this.mostrarFatura();
        }
      }
    )
  
    UnidadeSelectedService.putTituloPagina(this.titulo_pagina)

    ErrorInputService.emitirMensagemErro.subscribe(
      mensagem_error => {
        this.mostrarErrordeInput(true)
      }
    )
  }

  putValorDeposito(valor: string){
    this.valor_deposito = valor
    this.formDeposito.reset()
    this.formDeposito.value.valor_deposito = valor
  }

  validaValorDeposito(){
    if(this.formDeposito.value.valor_deposito>50 && this.formDeposito.value.valor_deposito.toString().indexOf(".")=== -1){
      this.formDeposito.value.valor_deposito = this.formDeposito.value.valor_deposito + ".00"}
      console.log(this.formDeposito.value.valor_deposito)
  }

  solicitarDeposito(){

    if(this.formDeposito.value.valor_deposito === null){
      this.putValorDeposito("50,00")
    }

    if(this.unidade_selecionada != undefined){
      if(parseFloat(this.formDeposito.value.valor_deposito) >= 50){
        this.mostrarErrordeInput(false)
        this.mostrarLoader(true,"Solicitando Depósito")

        let deposito : any = {
          id_unidade: this.unidade_selecionada.id,
          valor: this.formDeposito.value.valor_deposito
        }
        this.requests.depositarReservaDeTroco(deposito)
        .subscribe(
          res => {
            this.mostrarLoader(false,null)

            var resp_deposito : any = res
            if(resp_deposito.code === 200){
              this.putValorDeposito(null)
              this.ultimoDepositoModuleResponse.refreshLastDeposito(true)
              this.url_fatura = resp_deposito.url_fatura
            }else{
              this.url_fatura = null
            }
            this.putValorDeposito("50,00")

            this.mostrarResponseScreen(true, resp_deposito.code, resp_deposito.mensagem,"250px")
        })
      }else{
        this.mostrarErrordeInput(true);
        this.errorMensagemService.enviaMensagemErro("O valor do depósito deve ser maior que R$ 50,00.")
      }
    }else{
      this.mostrarErrordeInput(true);
      this.errorMensagemService.enviaMensagemErro("Você deve escolher uma unidade para realizar o depósito.")
    }
  }

  mostrarFatura(){
    window.open(this.url_fatura, '_blank');
  }

  mostrarErrordeInput(status :boolean){
    this.errors_input = status
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
