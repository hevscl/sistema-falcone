import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimoDepositoComponent } from './ultimo-deposito.component';

describe('UltimoDepositoComponent', () => {
  let component: UltimoDepositoComponent;
  let fixture: ComponentFixture<UltimoDepositoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UltimoDepositoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimoDepositoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
