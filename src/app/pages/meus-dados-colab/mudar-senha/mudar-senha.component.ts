import { Component, OnInit } from '@angular/core';
import { TogglePasswordService } from 'src/services/modules_services/toggle_password.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Requests } from 'src/services/requests.service';
declare var $;

@Component({
  selector: 'app-mudar-senha',
  templateUrl: './mudar-senha.component.html',
  styleUrls: ['./mudar-senha.component.css'],
  providers: [ 
    TogglePasswordService,
    ResponsesModuleService ]
})
export class MudarSenhaComponent implements OnInit {

  public responseScreen : boolean = false
  public estadoLoad : boolean = false

  public mensagem_loader : string = ""

  public formNewPassword : FormGroup = new FormGroup({
    'senha_atual' : new FormControl(null),
    'nova_senha' : new FormControl(null),
    'nova_senha_repeat' : new FormControl(null)
  });

  public show_password : boolean = false
  public show_new_password : boolean = false

  public togglePassword : boolean = false
  public toggleNewPassword : boolean = false
  public toggleNewRePassword : boolean = false

  public senhaAtualValida : boolean = false
  public senhaNovaValida : boolean = false
  public senhaNovaRepeatValida : boolean = false

  public estadoForm2 : string = "disabled"

  constructor(
    private requests : Requests,
    private togglePasswordService : TogglePasswordService,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
        }
      }
    )
  }

  alterarSenha(){
    this.mostrarLoader(true, "Mudando Senha")
    let dados_senha : any = {
      'senha_atual' : this.formNewPassword.value.senha_atual,
      'senha_nova' : this.formNewPassword.value.nova_senha
    }

    this.requests.atualizarSenhaPessoal(dados_senha).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_senha : any = res
        if(resp_senha.code===200){
          this.formNewPassword.reset()
          this.senhaAtualValida = false
          this.senhaNovaValida = false
          this.senhaNovaRepeatValida = false
          this.togglePassword = false
          this.toggleNewPassword = false
          this.toggleNewRePassword = false
        }
        
        this.mostrarResponseScreen(
          true,
          resp_senha.code,
          resp_senha.mensagem,
          "400px"
        )
      }
    )
  }

  validaSenha(){
    if(this.formNewPassword.value.senha_atual.length>=5){
      this.senhaAtualValida = true
    }else{
      this.senhaAtualValida = false
    }
    this.mostrarTogglePassword()
    this.ativarBotao2()
  }

  validaNovaSenha(){
    if(this.formNewPassword.value.nova_senha.length>=5){
      this.senhaNovaValida = true
    }else{
      this.senhaNovaValida = false
    }
    this.mostrarToggleNewPassword()
    this.ativarBotao2()
  }

  validaNovaSenhaRR(){
    if(this.formNewPassword.value.nova_senha_repeat.length>=5 
      && this.formNewPassword.value.nova_senha_repeat===this.formNewPassword.value.nova_senha){
      this.senhaNovaRepeatValida = true
    }else{
      this.senhaNovaRepeatValida = false
    }
    this.mostrarToggleNewRePassword()
    this.ativarBotao2()
  }

  ativarBotao2(){
    if(this.senhaAtualValida == true && this.senhaNovaValida == true && this.senhaNovaRepeatValida == true){
      this.estadoForm2 = ''
    }else{
      this.estadoForm2 = 'disabled'
    }
  }

  mostrarPassword(){
    this.show_password = !this.show_password
    if(this.show_password){
      this.togglePasswordService.enviaMostrarPassword(true)
    }else{
      this.togglePasswordService.enviaMostrarPassword(false)
    }
  }

  mostrarTogglePassword(){
    if(this.formNewPassword.value.senha_atual.length>0){
      this.togglePassword = true
    }else{
      this.togglePassword = false
    }
  }

  mostrarToggleNewPassword(){
    if(this.formNewPassword.value.nova_senha.length>0){
      this.toggleNewPassword = true
    }else{
      this.toggleNewPassword = false
    }
  }

  mostrarToggleNewRePassword(){
    if(this.formNewPassword.value.nova_senha_repeat.length>0){
      this.toggleNewRePassword = true
    }else{
      this.toggleNewRePassword = false
    }
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
