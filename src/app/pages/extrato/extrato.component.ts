import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-extrato',
  templateUrl: './extrato.component.html',
  styleUrls: ['./extrato.component.css']
})
export class ExtratoComponent implements OnInit {

  constructor(private rotas: Router) { }

  ngOnInit() {
  }

  tpassados(){
    this.rotas.navigate(['/trocos-passados']);
  }

  fcaixa(){
    this.rotas.navigate(['/fechamento-caixa']);
  }

  rinserida(){
    this.rotas.navigate(['/reservas-inseridas']);
  }

}
