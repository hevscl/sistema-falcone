import { OperadorSelectedService } from './../../../../services/modules_services/operador_selected.service';
import { InserviceService } from './../../../../services/inservice.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-operadormodal',
  templateUrl: './operadormodal.component.html',
  styleUrls: ['./operadormodal.component.css'],
  providers: [ OperadorSelectedService ]
})



export class OperadorModalComponent implements OnInit {
  idunidadeEscolhida: number = OperadorSelectedService.id_unidade_selected
  public data : any
  operadores: any
  operador_selected: any
  resposta_operadores :any = []

  ativo: boolean = false

  estadoLoad: boolean = true
  estadoOperadores: boolean
  estadoUnidade: boolean = true

  msg_no_operators : string

  id_operador_selecionado :number = 0

  constructor(
    private operadorSelectedService :OperadorSelectedService,
    private serv: InserviceService ) { }

  ngOnInit() {

    this.buscaOperador(this.idunidadeEscolhida)

    OperadorSelectedService.emitirUnidadeSelecionada.subscribe(
      id_unidade => {
        this.idunidadeEscolhida = id_unidade
        this.buscaOperador(id_unidade)
      }
    )
  }

  buscaOperador(id_unidade){
    this.estadoUnidade = false
    this.fecharLoad(true)
    this.serv.buscaListaOperador(id_unidade)
    .subscribe(
      dados => {
      this.estadoUnidade = true
      this.fecharLoad(false)
      if(dados.code == 200){
        this.operadores = dados.data
        this.estadoOperadores = true
      }else{
        this.operadores = null
        this.estadoOperadores = false
        this.msg_no_operators = dados.mensagem
      }
    })
  }

  mudarParametro(id_operador){
    this.operadorSelectedService.enviaOperadorSelecionado(id_operador)
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }

 
}