import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrocosPassadosComponent } from './trocos-passados.component';

describe('TrocosPassadosComponent', () => {
  let component: TrocosPassadosComponent;
  let fixture: ComponentFixture<TrocosPassadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrocosPassadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrocosPassadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
