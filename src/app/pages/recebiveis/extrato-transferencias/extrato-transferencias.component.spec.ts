import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtratoTransferenciasComponent } from './extrato-transferencias.component';

describe('ExtratoTransferenciasComponent', () => {
  let component: ExtratoTransferenciasComponent;
  let fixture: ComponentFixture<ExtratoTransferenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtratoTransferenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoTransferenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
