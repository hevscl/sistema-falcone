import { DataTableDirective } from 'angular-datatables';
import { ErrorInputService } from './../../../services/modules_services/error_input.service';
import { FormGroup, FormControl } from '@angular/forms';
import { UnidadeSelectedService } from './../../../services/unidade_selected.service';
import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { UnidadeSelected } from 'src/models/unidade_selected.model';

@Component({
  selector: 'app-recebiveis',
  templateUrl: './recebiveis.component.html',
  styleUrls: ['./recebiveis.component.css'],
  providers: [UnidadeSelectedService, 
    ErrorInputService]
})
export class RecebiveisComponent implements OnInit {

  public errors_input : boolean = false
  public estadoLoad : boolean = false
  public estadoLoadTransf : boolean = false
  public mensagem_loader : string
  public status_saldo : boolean = true
  msg_no_saldo : string
  public tela1 : boolean = false
  public tela2 : boolean = false
  public tela3 : boolean = false

  public saldo_exibir : any
  saldo : any
  taxa : any
  valor_solic : any
  valor_transferencia_taxada : any
  valor_taxa : any

  resp_transf : any

  public unidades : any
  public unidade_selecionada : any
  status_unidade_select :boolean = false;
  unidade_selecionada_nome : string = "Nenhuma Unidade Selecionada"

  public titulo_pagina : string = "Escolha a unidade para verificar os valores recebidos"

  public estadoEscolhaUnidade : boolean

  public formTransferenciaRecebiveis : FormGroup = new FormGroup({
    'valor' : new FormControl(null)
  })

  public dados_conta = JSON.parse(localStorage.getItem('dados_conta'))

  public colaborador = this.dados_conta.colaborador
  public id_unidade = this.colaborador[0].id_unidade
  public is_admin = this.colaborador[0].admin

  constructor(private requests : Requests,
    private errorMensagemService : ErrorInputService,
    private unidadeSelectedService : UnidadeSelectedService) { }

  ngOnInit() {
    UnidadeSelectedService.putTituloPagina(this.titulo_pagina)

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidadeSelected => {
        this.unidades = unidadeSelected
        this.unidade_selecionada = this.unidades.id
        this.mostrarErrordeInput(false)
        this.buscarSaldo(this.unidade_selecionada)
      }
    )

    

    if(this.is_admin===1){
      this.estadoEscolhaUnidade = true
    }else{
      this.estadoEscolhaUnidade = false
    }

    ErrorInputService.emitirMensagemErro.subscribe(
      mensagem_error => {
        this.mostrarErrordeInput(true)
      }
    )

  }

  validaValorTransferencia(){
    if(this.formTransferenciaRecebiveis.value.valor>0 && this.formTransferenciaRecebiveis.value.valor.toString().indexOf(".")=== -1){
      this.formTransferenciaRecebiveis.value.valor = this.formTransferenciaRecebiveis.value.valor + ".00"}
      console.log(this.formTransferenciaRecebiveis.value.valor)
  }

  buscarSaldo(id_unidade){
    this.mostrarLoader(true, "Buscando Saldo")
    this.mostrarUnidadeSelecionada(true)
    this.requests.saldoVoucher(id_unidade).subscribe(
      res => {
        this.mostrarLoader(false,"")
        var res_saldo : any = res
        
        if(res_saldo.code === 200){
          this.mostrarSaldo(true)
          this.saldo = res_saldo.data 
          this.saldo_exibir = this.saldo[0].disponivel
        }else{
          this.mostrarSaldo(false)
        }
      }
    )
  }

  calcularValorTransferencia(){
   if(this.unidade_selecionada != undefined){
      if(parseFloat(this.formTransferenciaRecebiveis.value.valor) <= this.saldo[0].disponivel){
        //this.mostrarErrordeInput(false)
        this.mostrarErrordeInput(false)
        this.mostrarLoaderT(true, "Solicitando Transferência")

        let calc : any = {
          valor_transferencia : this.formTransferenciaRecebiveis.value.valor
        }

        this.requests.calcularValor(calc).subscribe(
          res => {
            this.mostrarLoaderT(false,"")
            this.mostrarTela2(true)
            var resp_calc : any = res 
         
            if(resp_calc.code === 200){
              console.log(resp_calc)
              this.taxa = resp_calc.data.taxa
              this.valor_taxa = resp_calc.data.valor_taxa
              this.valor_transferencia_taxada = resp_calc.data.valor_transferencia
              this.valor_solic = calc.valor_transferencia
            }   
          }
        )
      }else{
        this.mostrarLoaderT(false,"")
        this.mostrarUnidadeSelecionada(true)
        this.mostrarErrordeInput(true)
        this.errorMensagemService.enviaMensagemErro("O valor deve corresponder ao saldo disponível")
      }
    }else{
      this.mostrarErrordeInput(true)
      this.errorMensagemService.enviaMensagemErro("Você deve escolher uma unidade.")
    }
  }

  solicitarTransferencia(){
    if(this.unidade_selecionada != undefined){
      if(parseFloat(this.formTransferenciaRecebiveis.value.valor) <= this.saldo[0].disponivel){
        this.mostrarErrordeInput(false)
        this.mostrarLoaderT(true, "Solicitando Transferência")
        let transf : any = {
          id_unidade: this.unidade_selecionada,
          valor_transferencia: this.formTransferenciaRecebiveis.value.valor
        }
        this.requests.tranferirSaldo(transf).subscribe(
          res => {
            this.mostrarLoaderT(false,"")
            this.resp_transf = res 
            console.log(this.resp_transf)
            if(this.resp_transf.code === 200){
              
              this.mostrarTela2(false)
              this.mostrarTela3(true)
            }
          }
        )
      }else{
        
      }
    }//else{
      
    //}
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  voltarTela1(){
    this.tela1 = true
    this.tela2 = false
    this.tela3 = false
  }

  mostrarTela1(status: boolean){
    this.tela1 = status
  }

  mostrarTela2(status: boolean){
    this.tela2 = status
  }

  mostrarTela3(status: boolean){
    this.tela3 = status
  }

  mostrarLoaderT(status1: boolean, mensagem: string){
    this.estadoLoadTransf = status1
    this.mensagem_loader = mensagem
  }

  mostrarErrordeInput(status :boolean){
    this.errors_input = status
  }

  mostrarUnidadeSelecionada(status : boolean) :void{
    this.status_unidade_select = status
  }

  mostrarSaldo(status :boolean){
    this.msg_no_saldo = "Não há saldo disponível para a unidade selecionada."
    this.status_saldo = status
  }
}
