import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtratoRecebiveisComponent } from './extrato-recebiveis.component';

describe('ExtratoRecebiveisComponent', () => {
  let component: ExtratoRecebiveisComponent;
  let fixture: ComponentFixture<ExtratoRecebiveisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtratoRecebiveisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoRecebiveisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
