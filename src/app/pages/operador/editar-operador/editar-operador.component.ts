import { TelefoneValidation } from './../../../../utils/valida-telefone';
import { ValidaCpf } from './../../../../utils/valida-cpf';
import { ResponsesModuleService } from './../../../../services/modules_services/responses_module.service';
import { ErrorInputService } from './../../../../services/modules_services/error_input.service';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UnidadeSelected } from 'src/models/unidade_selected.model';
import { EditOperadorService } from 'src/services/modules_services/edit_operador.service';
import { Router } from '@angular/router';
import { Requests } from 'src/services/requests.service';
declare var $: any;

@Component({
  selector: 'app-editar-operador',
  templateUrl: './editar-operador.component.html',
  styleUrls: ['./editar-operador.component.css'],
  providers: [
    AcessPagesService,
    EditOperadorService,
    UnidadeSelectedService,
    ErrorInputService,
    ResponsesModuleService,
    ValidaCpf,
    TelefoneValidation
  ]
})
export class EditarOperadorComponent implements OnInit {

  public dados : any = EditOperadorService.info_operador

  public estadoAcess : boolean = false

  public estadoForm : string = "disabled"

  public nomeValido : boolean = false
  public data_nascValido : boolean = false
  public cpfValido : boolean = false
  public telefoneValido : boolean = false

  public formEditarOperador :FormGroup = new FormGroup({
    'nome_operador' : new FormControl(null),
    'data_nascimento' : new FormControl(null),
    'cpf' : new FormControl(null),
    'telefone' : new FormControl(null)
  })

  public id_unidade : number
  public nome_unidade : string = "Unidade de Loja"

  public errors_input : boolean
  public estadoLoad : boolean = false
  public estadoLoadDelete : boolean = false
  public responseScreen : boolean = false
  public responseDeleteScreen : boolean = false

  public mensagem_loader : string = "Carregando Dados"

  public unidade_escolhida : UnidadeSelected

  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]
  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  constructor(private acessPagesService : AcessPagesService,
    private errorInputService : ErrorInputService,
    private requests : Requests,
    private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation,
    private responsesModuleService : ResponsesModuleService,
    private rotas : Router) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(this.responseDeleteScreen){
          $('#modalExcluir').modal('hide');
          this.rotas.navigate(["dashboard/gerenciar-operador"])
        }
      }
    )

    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Gerenciar operador de caixa",
        style: "link_blur",
        router: "gerenciar-operador"
      },
      {
        page: "Editar operador de caixa",
        style: "link_ativo",
        router: "editar-operador"
      }]
    )

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidade => {
        this.unidade_escolhida = unidade
        this.id_unidade = this.unidade_escolhida.id
        this.nome_unidade = this.unidade_escolhida.nome_fantasia   
        this.mostrarErrosInput(
          false,
          null
        )  
      }
    )

    this.setInfo()
  }

  setInfo(){
    console.log(this.dados)

    if(this.dados!=undefined){
      this.formEditarOperador.get('nome_operador').setValue(this.dados.nome)
      this.formEditarOperador.get('cpf').setValue(this.dados.cpf)
      this.formEditarOperador.get('data_nascimento').setValue(this.dados.data_nascimento)
      this.formEditarOperador.get('telefone').setValue(this.dados.celular)

      this.id_unidade = this.dados.id_unidade
      this.nome_unidade = this.dados.unidade

      this.validaNome("")
      this.validaCPF(this.formEditarOperador.get('cpf'))
      this.validaData("")
      this.validaTelefone(this.formEditarOperador.get('telefone'))
    }else{
      this.rotas.navigate(["/dashboard/gerenciar-operador"])
    }
  }

  mostrarErrosInput(status:boolean, mensagem: string){
    this.errors_input = status;
    this.errorInputService.enviaMensagemErro(mensagem);
  }

  mostrarLoaderDelete(status: boolean, mensagem: string){
    this.estadoLoadDelete = status
    this.mensagem_loader = mensagem
  }

  mostrarResponseDeleteScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseDeleteScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }


  editarOperador(){
    
  }

  deleteOperador(){
    this.mostrarLoaderDelete(true, "Excluindo Operador")
    var excluir_operador : any = {
      'id_operador' : this.dados.id
    }
    this.requests.excluirOperador(excluir_operador)
    .subscribe(
      res => {
        this.mostrarLoaderDelete(false, null)
        var resp_delete : any = res
        if(resp_delete.code===200){

        }else{

        }
        this.mostrarResponseDeleteScreen(
          true,
          resp_delete.code,
          resp_delete.mensagem,
          "300px"
        )
      }
    )
  }

  validaNome(nome : string){
    if(this.formEditarOperador.value.nome_operador.length<7 || this.formEditarOperador.value.nome_operador.indexOf(" ") == -1){
      this.nomeValido = false
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formEditarOperador.value.data_nascimento.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(cpf.value)
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(telefone.value)
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido == true && this.cpfValido == true && this.data_nascValido == true && this.telefoneValido == true){
        this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

}
