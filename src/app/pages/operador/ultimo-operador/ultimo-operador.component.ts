import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { LastOperador } from 'src/models/last_operador.model';
import { UltimoOperadorModuleService } from 'src/services/modules_services/ultimo_operador_module.service';

@Component({
  selector: 'app-ultimo-operador',
  templateUrl: './ultimo-operador.component.html',
  styleUrls: ['./ultimo-operador.component.css'],
  providers: [ UltimoOperadorModuleService ]
})
export class UltimoOperadorComponent implements OnInit {

  public estadoLoad : boolean = true
  public status_last_operador : boolean = true

  last_operador : LastOperador = {
    nome: "",
    cpf: 0,
    unidade: "",
    senha: ""
  }

  constructor(private requests : Requests) { }

  ngOnInit() {
    UltimoOperadorModuleService.emitirRefreshOperador.subscribe(
      trigger => {
      if(trigger === true){
        this.solicitarUltimoOperador()}
      }
    )

      this.solicitarUltimoOperador()
  }

  solicitarUltimoOperador(){
    this.requests.solicitarUltimoOperador()
    .subscribe(
      res => {
        this.fecharLoad(false)
        this.status_last_operador = true
        var resposta_last_operador : any = res
        console.log(resposta_last_operador)
        if(resposta_last_operador != null){
          if(resposta_last_operador.code === 200){
            this.status_last_operador = true
            this.last_operador = resposta_last_operador.data
          }else{
            this.status_last_operador = false
          }
        }
      }
    )
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }

}
