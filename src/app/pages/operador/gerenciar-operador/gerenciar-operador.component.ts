import { UnidadeSelectedService } from './../../../../services/unidade_selected.service';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gerenciar-operador',
  templateUrl: './gerenciar-operador.component.html',
  styleUrls: ['./gerenciar-operador.component.css'],
  providers: [
    UnidadeSelectedService, 
    AcessPagesService
  ]
})
export class GerenciarOperadorComponent implements OnInit {
  public estadoAcess : boolean = false

  public titulo_pagina : string = "Escolha a unidade do operador"

  constructor(private acessPagesService : AcessPagesService) { }

  ngOnInit() {
    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Cadastrar Operador",
        style: "link_blur",
        router: "operador"
      },
      {
        page: "Gerenciar operadores de caixa",
        style: "link_ativo",
        router: "gerenciar-operador"
      }]
    )

    UnidadeSelectedService.putTituloPagina(this.titulo_pagina)
  }


}
