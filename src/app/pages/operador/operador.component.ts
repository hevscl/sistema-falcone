import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Unidades } from './../../../models/unidade.model';
import { TelefoneValidation } from './../../../utils/valida-telefone';
import { ValidaCpf } from './../../../utils/valida-cpf';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';
import { Requests } from 'src/services/requests.service';
import { AddOperador } from 'src/models/add_operador.model';
import { ErrorInputService } from 'src/services/modules_services/error_input.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { UnidadeSelected } from 'src/models/unidade_selected.model';
import { UltimoOperadorModuleService } from 'src/services/modules_services/ultimo_operador_module.service';
 
@Component({
  selector: 'app-operador',
  templateUrl: './operador.component.html',
  styleUrls: ['./operador.component.css'],
  providers: [
    UnidadeSelectedService,
    ErrorInputService,
    ResponsesModuleService,
    UltimoOperadorModuleService,
    ValidaCpf,
    TelefoneValidation
  ]
})
export class OperadorComponent implements OnInit {

  public estadoForm : string = "disabled"

  public nomeValido : boolean = false
  public data_nascValido : boolean = false
  public cpfValido : boolean = false
  public telefoneValido : boolean = false

  public formOperador :FormGroup = new FormGroup({
    'nome_operador' : new FormControl(null),
    'data_nascimento' : new FormControl(null),
    'cpf' : new FormControl(null),
    'telefone' : new FormControl(null)
  })

  public id_unidade : number
  public nome_unidade : string = "Unidade de Loja"

  public unidade_escolhida : UnidadeSelected

  public errors_input : boolean
  public estadoLoad : boolean = false
  public responseScreen : boolean = false

  public mensagem_loader : string

  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]
  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  constructor(
    private requests : Requests,
    private errorInputService : ErrorInputService,
    private responsesModuleService : ResponsesModuleService,
    private ultimoOperadorModuleService : UltimoOperadorModuleService,
    private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation
    ) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      close => this.mostrarResponseScreen(false, null, null, "272px")
    )

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidade => {
        this.unidade_escolhida = unidade
        this.id_unidade = this.unidade_escolhida.id
        this.nome_unidade = this.unidade_escolhida.nome_fantasia   
        this.mostrarErrosInput(
          false,
          null
        )  
      }
    )
  }

  cadastrarOperador(){

    if(this.unidade_escolhida != undefined){
      this.mostrarLoader(true, "Cadastrando Operador")

      let new_operador = new AddOperador(
        this.id_unidade,
        this.formOperador.value.nome_operador,
        this.formOperador.value.cpf,
        this.formOperador.value.data_nascimento,
        this.formOperador.value.telefone
        )
  
      this.requests.cadastrarOperador(new_operador)
      .subscribe(
        res => {
          this.mostrarLoader(false, null)
          var resp_add_operador : any = res
          if(resp_add_operador.data != null){
            if(resp_add_operador.code === 200){
              this.ultimoOperadorModuleService.refreshLastOperador(true)
              this.formOperador.reset()
            }else{
            }

            this.mostrarResponseScreen(true,
              resp_add_operador.code,
              resp_add_operador.mensagem,
              "272px")
          }
        }
      )

      this.mostrarErrosInput(
        false,
        null
      )
    }else{
      this.mostrarErrosInput(
        true,
        "Escolha uma unidade a qual o operador pertence"
      )
    }   
  }


    mostrarErrosInput(status:boolean, mensagem: string){
      this.errors_input = status;
      this.errorInputService.enviaMensagemErro(mensagem);
    }
  
    mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
      this.responseScreen = status
      this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
    }
  
    mostrarLoader(status: boolean, mensagem: string){
      this.estadoLoad = status
      this.mensagem_loader = mensagem
    }

    validaNome(nome : string){
      if(this.formOperador.value.nome_operador.length<7 || this.formOperador.value.nome_operador.indexOf(" ") == -1){
        this.nomeValido = false
      }else{
        this.nomeValido = true
      }
      this.ativarBotao()
    }

    validaData(data){
      var dt = this.formOperador.value.data_nascimento.replace("/","").replace("/","")
      var dataFinal = dt.replace(/_/g,"")
      if(dataFinal.toString().length == 8){
        var dia = dataFinal.substring(0,2)
        var mes = dataFinal.substring(2,4)
        var ano = dataFinal.substring(4)
        var ano_atual = new Date
        var ano_atual_final = ano_atual.getFullYear()-18
        if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
          this.data_nascValido = true
        }else{
          this.data_nascValido = false
        }
      }else{
        this.data_nascValido = false
      }
      this.ativarBotao()
    }

    validaCPF(cpf){
      this.cpfValido = this.validaCpf.validarCPF(cpf.value)
      this.ativarBotao()
    }

    validaTelefone(telefone){
      this.telefoneValido = this.validaTelef.validarTel(telefone.value)
      this.ativarBotao()
    }

    ativarBotao(){
      if(this.nomeValido == true && this.cpfValido == true && this.data_nascValido == true && this.telefoneValido == true){
          this.estadoForm = ''
      }else{
        this.estadoForm = 'disabled'
      }
    }
    
}
