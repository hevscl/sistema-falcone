import { InserviceService } from './../../../../services/inservice.service';
import { OperadorSelectedService } from './../../../../services/modules_services/operador_selected.service';
import { Component, OnInit } from '@angular/core';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';
import { Requests } from 'src/services/requests.service';
import { Router } from '@angular/router';
import { EditOperadorService } from 'src/services/modules_services/edit_operador.service';

@Component({
  selector: 'app-operadorescadastrados',
  templateUrl: './operadorescadastrados.component.html',
  styleUrls: ['./operadorescadastrados.component.css'],
  providers: [ 
    OperadorSelectedService,
    UnidadeSelectedService,
    EditOperadorService ]
})
export class OperadorescadastradosComponent implements OnInit {
  idunidadeEscolhida: number
  public data : any
  operadores: any
  operador_selected: any
  resposta_operadores :any = []

  ativo: boolean = false

  estadoLoad: boolean = false
  estadoOperadores: boolean
  estadoUnidade: boolean = false


  msg_no_operators : string

  id_operador_selecionado :number = 0

  constructor(
    private operadorSelectedService :OperadorSelectedService,
    private editOperadorService : EditOperadorService,
    private serv: InserviceService,
    private requests : Requests,
    private rotas : Router ) { }

  ngOnInit() {
    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidade => {
        this.estadoUnidade = true
        this.idunidadeEscolhida = unidade.id
        this.buscaOperador(this.idunidadeEscolhida)
      }
    )

    this.buscaOperador(this.idunidadeEscolhida)

    OperadorSelectedService.emitirUnidadeSelecionada.subscribe(
      id_unidade => {
        this.idunidadeEscolhida = id_unidade
        this.buscaOperador(id_unidade)
      }
    )
  }

  buscaOperador(id_unidade){
    this.fecharLoad(true)
    if(id_unidade != null){
      this.serv.buscaListaOperador(id_unidade)
      .subscribe(
        dados => {
        this.fecharLoad(false)
        if(dados.code == 200){
          this.operadores = dados.data
          this.estadoOperadores = true
        }else{
          this.operadores = null
          this.estadoOperadores = false
          this.msg_no_operators = dados.mensagem
        }
      });
    }else{
      this.fecharLoad(false)
    }
  }

  onChange(evento, id_unidade, id_operador){
    var status = evento===true?1:0

    var dados_ativar = {
      id_unidade : id_unidade,
      id_operador : id_operador,
      status: status
    }

    this.requests.ativarOperador(dados_ativar)
    .subscribe(
      res => {
        var resp_ativar : any = res
        if(resp_ativar.code===200){
          this.serv.buscaListaOperador(id_unidade)
          .subscribe(
            dados => this.operadores = dados.data
          )
        }
      }
    )

  }

  editarOperador(operador){
    this.editOperadorService.enviaInfoOperadorConfig(operador)
    this.rotas.navigate(['dashboard/editar-operador'])
  }

  mudarParametro(id_operador){
    this.operadorSelectedService.enviaOperadorSelecionado(id_operador)
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }
}
