import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ValidaCpf } from './../../../../utils/valida-cpf';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EditClienteService } from 'src/services/modules_services/edit_cliente.service';
import { Router } from '@angular/router';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { Requests } from 'src/services/requests.service';
declare var $;

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css'],
  providers: [ValidaCpf,
    TelefoneValidation,
    EditClienteService,
    ResponsesModuleService]
})
export class EditarClienteComponent implements OnInit {

  public estadoLoad : boolean = false
  public responseScreen : boolean = false

  public mensagem_loader : string

  public cliente = EditClienteService.cliente
  public id_cliente : number

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public formEditCliente :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'endereco' : new FormControl(null),
    'email' : new FormControl(null),
    'telefone' : new FormControl(null),
    'data_nasc' : new FormControl(null)
  })

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public enderecoValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public data_nascValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(
    private requests : Requests,
    private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation,
    private rotas : Router,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
        }
      }
    )

    this.setInfo()
  }

  setInfo() {
    if (this.cliente != undefined && this.cliente != null) {
      this.formEditCliente.get('nome').setValue(this.cliente.nome)
      this.formEditCliente.get('cpf').setValue(this.cliente.cpf)
      this.formEditCliente.get('email').setValue(this.cliente.email)
      this.formEditCliente.get('telefone').setValue(this.cliente.celular)
      this.formEditCliente.get('endereco').setValue(this.cliente.endereco)
      this.formEditCliente.get('data_nasc').setValue(this.cliente.data_nascimento)

      this.id_cliente = this.cliente.id

      this.validaNome("")
      this.validaCPF(this.formEditCliente.get('cpf'))
      this.validaEmail("")
      this.validaData("")
      this.validaTelefone(this.formEditCliente.get('telefone'))
      this.validaEndereco("")
    }else{
      this.rotas.navigate(["/dashboard/gerenciar"])
    }
  }

  atualizarCliente(){
    this.mostrarLoader(true, "Atualizando Cliente");
    let dados_cliente : any = {
      'id_cliente' : this.id_cliente,
      'nome' : this.formEditCliente.value.nome,
      'cpf' : this.formEditCliente.value.cpf,
      'celular' : this.formEditCliente.value.telefone,
      'email' : this.formEditCliente.value.email,
      'endereco' : this.formEditCliente.value.endereco,
      'data_nascimento' : this.formEditCliente.value.data_nasc,
      'cep' : "",
      'numero' : ""
    }

    console.log("Email", this.formEditCliente.value.email)

    this.requests.atualizarCliente(dados_cliente).subscribe(
      res => {
        this.mostrarLoader(false, null);
        var resp_cliente : any = res
        if(resp_cliente.code===200){

        }else{

        }
        this.mostrarResponseScreen(
          true,
          resp_cliente.code,
          resp_cliente.mensagem,
          "400px"
        )
      }
    )
  }

  validaNome(nome : string){
    if(this.formEditCliente.value.nome.length<7 || this.formEditCliente.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formEditCliente.value.cpf)
    this.ativarBotao()
  }


  validaEndereco(endereco : string){
    if(this.formEditCliente.value.endereco.length>5){
      this.enderecoValido = true
      
    }else{
      this.enderecoValido = false
    }
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formEditCliente.value.email.indexOf('@') == -1 || this.formEditCliente.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(this.formEditCliente.value.telefone)
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formEditCliente.value.data_nasc.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.telefoneValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
