import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EditClienteService } from 'src/services/modules_services/edit_cliente.service';
import { ListaBuscaClienteService } from 'src/services/modules_services/lista_busca_cliente.service';

@Component({
  selector: 'app-lista-cliente',
  templateUrl: './lista-cliente.component.html',
  styleUrls: ['./lista-cliente.component.css'],
  providers: [ 
    ListaBuscaClienteService,
    EditClienteService ]
})
export class ListaClienteComponent implements OnInit {

  public clientes : any = ListaBuscaClienteService.cliente

  constructor(
    private rotas : Router,
    private editClienteService : EditClienteService) { }

  ngOnInit() {
  }

  mostrarImg(img : String){
    if(img!=null){
      return img
    }else{
      return "/assets/new_user_img.svg"
    }
  }

  editarCliente(cliente){
    this.editClienteService.enviaEditCliente(cliente)
    this.rotas.navigate(['dashboard/editar-cliente'])
  }

}
