import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ValidaCpf } from 'src/utils/valida-cpf';
import { Requests } from 'src/services/requests.service';
import { TelefoneValidation } from 'src/utils/valida-telefone';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
declare var $;

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
  providers: [ValidaCpf,
    TelefoneValidation,
    ResponsesModuleService]
})
export class ClientesComponent implements OnInit {
  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public birth_mask = [/\d/, /\d/,'/', /\d/, /\d/, '/', /\d/,/\d/,/\d/,/\d/]

  public formCliente :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'endereco' : new FormControl(null),
    'email' : new FormControl(null),
    'telefone' : new FormControl(null),
    'data_nasc' : new FormControl(null)
  })

  public cliente : any
  public id_cliente : number

  public search_cpf : boolean

  public estadoLoad : boolean = false
  public responseScreen : boolean =  false

  public mensagem_loader : string

  public text_button : string = "CADASTRAR CLIENTE"
  public cliente_ja_cadastrado : boolean
  public mensagem_status_cliente : string = "Esse cliente já se encontra cadastrado. Se necessário, atualize este cliente."

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public enderecoValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public data_nascValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          if(this.responseScreen){
            this.mostrarResponseScreen(false, null, null, null)
          }
        }
      }
    )

    this.setInfo()
  }

  setInfo() {
    if (this.cliente != undefined && this.cliente != null) {
      this.formCliente.get('nome').setValue(this.cliente.nome)
      this.formCliente.get('email').setValue(this.cliente.email)
      this.formCliente.get('telefone').setValue(this.cliente.celular)
      this.formCliente.get('endereco').setValue(this.cliente.endereco)
      this.formCliente.get('data_nasc').setValue(this.cliente.data_nascimento)

      this.id_cliente = this.cliente.id

      this.validaNome("")
      this.validaCPF(this.formCliente.get('cpf'))
      this.validaEmail("")
      this.validaData("")
      this.validaTelefone(this.formCliente.get('telefone'))
      this.validaEndereco("")
    }else{
      this.formCliente.get('nome').setValue("")
      this.formCliente.get('email').setValue("")
      this.formCliente.get('telefone').setValue("")
      this.formCliente.get('endereco').setValue("")
      this.formCliente.get('data_nasc').setValue("")

      this.cpfValido = false
      this.validaNome("")
      this.validaEmail("")
      this.validaTelefone(this.formCliente.get('telefone'))
      this.validaEndereco("")
      this.validaData("")
    }
  }

  mostrarClienteJaCadastrado(status : boolean){
    this.cliente_ja_cadastrado = status
    if(status){
      this.text_button = "ATUALIZAR CLIENTE"
    }else{
      this.text_button = "CADASTRAR CLIENTE"
    }
    this.setInfo()
  }

  actionCliente(){
    if(this.cliente!=null){
      console.log("Veio pra cá")
      this.atualizarCliente()
    }else{
      this.addCliente()
    }
  }

  addCliente(){
    this.mostrarLoader(true, "Cadastrando Cliente");
    let dados_cliente : any = {
      'nome' : this.formCliente.value.nome,
      'cpf' : this.formCliente.value.cpf,
      'celular' : this.formCliente.value.telefone,
      'email' : this.formCliente.value.email,
      'endereco' : this.formCliente.value.endereco,
      'data_nascimento' : this.formCliente.value.data_nasc,
      'cep' : "",
      'numero' : ""
    }

    this.requests.cadastrarCliente(dados_cliente).subscribe(
      res => {
        this.mostrarLoader(false, null);
        var resp_cliente : any = res
        if(resp_cliente.code===200){
          this.formCliente.reset()
        }else{

        }
        this.mostrarResponseScreen(
          true,
          resp_cliente.code,
          resp_cliente.mensagem,
          "400px"
        )
      }
    )
  }

  atualizarCliente(){
    this.mostrarLoader(true, "Atualizando Cliente");
    let dados_cliente : any = {
      'id_cliente' : this.id_cliente,
      'nome' : this.formCliente.value.nome,
      'cpf' : this.formCliente.value.cpf,
      'celular' : this.formCliente.value.telefone,
      'email' : this.formCliente.value.email,
      'endereco' : this.formCliente.value.endereco,
      'data_nascimento' : this.formCliente.value.data_nasc,
      'cep' : "",
      'numero' : ""
    }

    console.log("Email", this.formCliente.value.email)

    this.requests.atualizarCliente(dados_cliente).subscribe(
      res => {
        this.mostrarLoader(false, null);
        var resp_cliente : any = res
        if(resp_cliente.code===200){
          this.cliente = null
          this.formCliente.reset()
          this.mostrarClienteJaCadastrado(false)
        }else{

        }
        this.mostrarResponseScreen(
          true,
          resp_cliente.code,
          resp_cliente.mensagem,
          "400px"
        )
      }
    )
  }

  buscarCPF(){
    if(this.search_cpf){
      let dados_cliente : any = {
        'cpf' : this.formCliente.value.cpf
      }
      this.requests.buscarClienteCPF(dados_cliente).subscribe(
        res => {
          var resp_cliente : any = res
          console.log(resp_cliente)
          if(resp_cliente.code===200){
            this.cliente = resp_cliente.data[0]
            this.mostrarClienteJaCadastrado(true)
          }else{
            this.cliente = null
            this.mostrarClienteJaCadastrado(false)
          }
        }
      )
    }
  }

  validaNome(nome : string){
    if(this.formCliente.value.nome.length<7 || this.formCliente.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formCliente.value.cpf)
    if(this.cpfValido){
      console.log("Aqui")
      this.buscarCPF()
      this.search_cpf = false
    }else{
      this.cliente = null
      this.search_cpf = true
      this.setInfo()
    }
    this.ativarBotao()
  }


  validaEndereco(endereco : string){
    if(this.formCliente.value.endereco.length>5){
      this.enderecoValido = true
      
    }else{
      this.enderecoValido = false
    }
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formCliente.value.email.indexOf('@') == -1 || this.formCliente.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(this.formCliente.value.telefone)
    this.ativarBotao()
  }

  validaData(data){
    var dt = this.formCliente.value.data_nasc.replace("/","").replace("/","")
    var dataFinal = dt.replace(/_/g,"")
    if(dataFinal.toString().length == 8){
      var dia = dataFinal.substring(0,2)
      var mes = dataFinal.substring(2,4)
      var ano = dataFinal.substring(4)
      var ano_atual = new Date
      var ano_atual_final = ano_atual.getFullYear()-18
      if(dia<=31 && mes<=12 && ano<=ano_atual_final && ano>1900){
        this.data_nascValido = true
      }else{
        this.data_nascValido = false
      }
    }else{
      this.data_nascValido = false
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.telefoneValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
