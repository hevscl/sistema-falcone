import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Requests } from '../../../services/requests.service';
import { FormGroup, FormControl } from '@angular/forms';
import { DadosAdmin } from 'src/models/dados_admin.model';
import { TelefoneValidation } from './../../../utils/valida-telefone';
import { ValidaCpf } from './../../../utils/valida-cpf';
import { ValidaCnpj } from './../../../utils/valida-cnpj';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { Senhas } from 'src/models/senhas.model';
declare var $: any;

@Component({
  selector: 'app-gerencia',
  templateUrl: './gerencia.component.html',
  styleUrls: ['./gerencia.component.css'],
  providers: [ ValidaCpf,
    TelefoneValidation,
    ValidaCnpj,
    ResponsesModuleService]
})
export class GerenciaComponent implements OnInit {

  public estadoForm : string = "disabled"
  public estadoForm2 : string = "disabled"
  public estadoForm3 : string = "disabled"

  public nomeValido : boolean = false
  public emailValido : boolean = false
  public telefoneValido : boolean = false
  public celularValido : boolean = false
  public cpfValido : boolean = false

  public nomeFantasiaValido : boolean = false
  public cnpjValido : boolean = false
  public ruaValido : boolean = false
  public bairroValido : boolean = false
  public cepValido : boolean = false
  public numeroValido : boolean = false
  public cidadeValido : boolean = false
  public estadoValido : boolean = false

  public senhaAtualValida : boolean = false
  public senhaNovaValida : boolean = false
  public senhaNovaRepeatValida : boolean = false

  public dados_conta = JSON.parse(localStorage.getItem('dados_conta'))

  public colaborador = this.dados_conta.colaborador
  public is_admin = this.colaborador[0].admin

  public gerencia_nivel_1 : boolean = false

  dados_admin_master : any = {}
  dados_empresa : any = {}

  estadoLoad : boolean = true
  estadoLoadDadosAdmin : boolean = false
  estadoLoadAlterarSenha : boolean = false
  estadoMudarFoto : boolean = true

  inputsDadosAdmin : boolean = false
  inputsDadosNegocio : boolean = false
  inputsAlterarSenha : boolean = true

  responseScreenSenha : boolean = false

  mensagem_loader : string = "Carregando Dados"

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public telefone_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public cep_mask = [/\d/,/\d/,/\d/,/\d/,/\d/, '-', /\d/, /\d/, /\d/]
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]
  public cnpj_mask = [/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/, '/', /\d/, /\d/, /\d/, /\d/,'-',/\d/,/\d/,]

  public formDadosAdmin :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'email'  : new FormControl(null),
    'cpf'  : new FormControl(null),
    'celular' : new FormControl(null),
    'telefone' : new FormControl(null)
  })

  public formDadosNegocio : FormGroup = new FormGroup({
    'nome_fantasia' : new FormControl(null),
    'cnpj' : new FormControl(null),
    'rua' : new FormControl(null),
    'numero' : new FormControl(null),
    'bairro' : new FormControl(null),
    'cep' : new FormControl(null),
    'cidade' : new FormControl(null),
    'estado' : new FormControl(null)
  })

  public formNewPassword : FormGroup = new FormGroup({
    'senha_atual' : new FormControl(null),
    'nova_senha' : new FormControl(null),
    'nova_senha_repeat' : new FormControl(null)
  })

  resposta_dados_admin_changed : any

  constructor(private rotas :Router, 
    private requests :Requests,
    private validaCpf : ValidaCpf,
    private validaTelef : TelefoneValidation,
    private validaCnpj : ValidaCnpj,
    private responsesModuleService : ResponsesModuleService) { }



  ngOnInit() {

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(this.responseScreenSenha){
          this.mostrarResponseSenhaScreen(false, true, null, null, null)
        }else{
          $('#exampleModall').modal('hide');
          this.solicitarDadosConta()
        }
      }
    )

    if(this.is_admin===1){
      this.gerencia_nivel_1 = true
    }else{
      this.gerencia_nivel_1 = false
    }

    this.solicitarDadosConta()

  }

  solicitarDadosConta(){
    this.exibirLoad(true, false, "Carregando Dados")
    this.requests.solicitarDadosConta()
    .subscribe(
      data => {

        this.exibirLoad(false, true, null)
        if(data!=null){
          var resposta :any = data
          if(resposta.code===200){
            this.dados_admin_master = resposta.data.admin_master
            this.dados_empresa = resposta.data.dados_negocio
            console.log(this.dados_empresa)

            if(resposta.data.dados_negocio.img_aprov === 0){
              this.mostrarMudarFoto(true)
            }else{
              this.mostrarMudarFoto(false)
            }

            this.formDadosAdmin.get('nome').setValue(this.dados_admin_master.nome_admin)
            this.formDadosAdmin.get('cpf').setValue(this.dados_admin_master.cpf_admin)
            this.formDadosAdmin.get('email').setValue(this.dados_admin_master.email_admin)
            this.formDadosAdmin.get('celular').setValue(this.dados_admin_master.celular_admin)
            this.formDadosAdmin.get('telefone').setValue(this.dados_admin_master.telefone_empresa)

            this.formDadosNegocio.get('cnpj').setValue(this.dados_empresa.cnpj_cpf)
            this.formDadosNegocio.get('nome_fantasia').setValue(this.dados_empresa.nome_fantasia)
            this.formDadosNegocio.get('cep').setValue(this.dados_empresa.cep)
            this.formDadosNegocio.get('rua').setValue(this.dados_empresa.endereco)
            this.formDadosNegocio.get('bairro').setValue(this.dados_empresa.bairro)
            this.formDadosNegocio.get('numero').setValue(this.dados_empresa.numero)
            this.formDadosNegocio.get('cidade').setValue(this.dados_empresa.cidade)
            this.formDadosNegocio.get('estado').setValue(this.dados_empresa.estado)

            this.validaTudo()
          }else{

          }
        }
      }
    )
  }

  mudarDadosAdmin(){
    this.mostrarDadosAdminLoad(true, false, "Atualizando Dados")

    let dadosAdmin : DadosAdmin = new DadosAdmin(
      this.formDadosAdmin.value.nome,
      this.formDadosAdmin.value.email,
      this.formDadosAdmin.value.cpf,
      this.formDadosAdmin.value.celular,
      this.formDadosAdmin.value.telefone
    ) 

    this.requests.mudarDadosAdmin(dadosAdmin)
    .subscribe(
      res => {
        this.mostrarDadosAdminLoad(false, true, null)
        if(res != null){
          this.resposta_dados_admin_changed = res;
          if(this.resposta_dados_admin_changed.code === 200){

            this.solicitarDadosConta()

          }
        }
      }
    )
  }

  atualizarSenhaNegocio(){
    this.mostrarAlterarSenhaLoad(true, false, "Alterando senha")
    var senhas = new Senhas(
      this.formNewPassword.value.senha_atual,
      this.formNewPassword.value.nova_senha
    );

    this.requests.mudarSenhaAdmin(senhas).subscribe(
      res => {
        this.mostrarAlterarSenhaLoad(false, true, null)
        var resp_atualiza_senha : any = res
        if(resp_atualiza_senha != null){
          if(resp_atualiza_senha.code === 200){
            
          }else{

          }

          this.mostrarResponseSenhaScreen(
            true,
            false,
            resp_atualiza_senha.code,
            resp_atualiza_senha.mensagem,
            "200px")
        }
      }
    )
  }

  navegarParaSubRotas(pagina: string) :void {
    this.rotas.navigate(["/dashboard/"+pagina])
  }

  exibirLoad(status :boolean, inverso :boolean, msg_loader : string){
    this.estadoLoad = status
    this.mensagem_loader = msg_loader
    this.mostrarInputs(inverso)
  }

  mostrarDadosAdminLoad(status: boolean, inverso :boolean, msg_loader: string){
    this.estadoLoadDadosAdmin = status
    this.mensagem_loader = msg_loader
    this.inputsDadosAdmin = inverso
  }

  mostrarAlterarSenhaLoad(status: boolean, inverso: boolean, msg_loader: string){
    this.estadoLoadAlterarSenha = status
    this.mensagem_loader = msg_loader
    this.inputsAlterarSenha = inverso
  }

  mostrarResponseSenhaScreen(status: boolean, inverso: boolean, code: number, mensagem: string, altura: string){
    this.responseScreenSenha = status
    this.inputsAlterarSenha = inverso
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarMudarFoto(status: boolean){
    this.estadoMudarFoto = status
  }

  mostrarInputs(inversoOfLoad : boolean){
    this.inputsDadosAdmin = inversoOfLoad
    this.inputsDadosNegocio = inversoOfLoad
    this.inputsAlterarSenha = inversoOfLoad
  }

  validaTudo(){
    this.validaNome("")
    this.validaCPF(this.formDadosAdmin.get('cpf'))
    this.validaEmail("")
    this.validaTelefone(this.formDadosAdmin.get('celular'))
    this.validaCelular(this.formDadosAdmin.get('celular'))
    this.estadoForm = 'disabled'

    this.validaCNPJ(this.formDadosNegocio.get('cnpj'))
    this.validaNomeFantasia("")
    this.validaCep("")
    this.validaRua("")
    this.validaBairro("")
    this.validaNumero("")
    this.validaCidade("")
    this.validaEstado("")
    this.estadoForm2 = 'disabled'
  }

  validaNome(nome : string){
    if(this.formDadosAdmin.value.nome.length<7 || this.formDadosAdmin.value.nome.indexOf(" ") == -1){
      this.nomeValido = false
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
    console.log(this.formDadosAdmin.value.nome)
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(cpf.value)
    this.ativarBotao()
  }

  validaTelefone(telefone){
    this.telefoneValido = this.validaTelef.validarTel(telefone.value)
    this.ativarBotao()
  }

  validaCelular(celular){
    this.celularValido = this.validaTelef.validarTel(celular.value)
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formDadosAdmin.value.email.indexOf('@') == -1 || this.formDadosAdmin.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido == true && this.cpfValido == true && this.emailValido && this.celularValido 
      && this.telefoneValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  validaNomeFantasia(nome : string){
    if(this.formDadosNegocio.value.nome_fantasia.length>5){
      this.nomeFantasiaValido = true
    }else{
      this.nomeFantasiaValido = false
    }
    this.ativarBotao2()
  }

  validaCNPJ(cnpj){
    this.cnpjValido = this.validaCnpj.validarCNPJ(cnpj.value)
    this.ativarBotao2()
  }

  validaBairro(bairro : string){
    if(this.formDadosNegocio.value.bairro.length>4){
      this.bairroValido = true
      
    }else{
      this.bairroValido = false
    }
    this.ativarBotao2()
  }

  validaRua(rua : string){
    if(this.formDadosNegocio.value.rua.length>1){
      this.ruaValido = true
      
    }else{
      this.ruaValido = false
    }
    this.ativarBotao2()
  }

  validaCidade(cidade : string){
    if(this.formDadosNegocio.value.cidade.length>2){
      this.cidadeValido = true
      
    }else{
      this.cidadeValido = false
    }
    this.ativarBotao2()
  }

  validaEstado(cidade : string){
    if(this.formDadosNegocio.value.estado.length == 2){
      this.estadoValido = true
      
    }else{
      this.estadoValido = false
    }
    this.ativarBotao2()
  }

  validaNumero(numero){
    if(this.formDadosNegocio.value.numero>0){
      this.numeroValido = true
    }else{
      this.numeroValido = false
    }
    this.ativarBotao2()
  }

  validaCep(cep){
    cep = this.formDadosNegocio.value.cep.replace("-","")
    var cepFinal = cep.replace(/_/g,"")
      if(cepFinal.toString().length == 8){
        this.cepValido = true
      }else{
        this.cepValido = false
      }
      this.ativarBotao2()
  }

  ativarBotao2(){
    if(this.nomeFantasiaValido == true && this.cepValido == true && this.bairroValido == true 
      && this.ruaValido == true && this.cidadeValido == true && this.estadoValido == true){
      this.estadoForm2 = ''
    }else{
      this.estadoForm2 = 'disabled'
    }
  }

  validaSenha(senha: string){
    if(this.formNewPassword.value.senha_atual.length>7){
      this.senhaAtualValida = true
    }else{
      this.senhaAtualValida = false
    }
    this.ativarBotao3()
  }

  validaNovaSenha(novasenha: string){
    if(this.formNewPassword.value.nova_senha.length>7){
      this.senhaNovaValida = true
    }else{
      this.senhaNovaValida = false
    }
    this.ativarBotao3()
  }

  validaNovaSenhaRR(novasenharepeat: string){
    if(this.formNewPassword.value.nova_senha_repeat.length>7){
      this.senhaNovaRepeatValida = true
    }else{
      this.senhaNovaRepeatValida = false
    }
    this.ativarBotao3()
  }

  ativarBotao3(){
    if(this.senhaAtualValida == true && this.senhaNovaValida == true && this.senhaNovaRepeatValida == true){
      this.estadoForm3 = ''
    }else{
      this.estadoForm3 = 'disabled'
    }
  }
}
