import { Mensalidade } from '../../../../../models/mensalidade.model'
import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { InserviceService } from 'src/services/inservice.service';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';

@Component({
  selector: 'app-mensalidades',
  templateUrl: './mensalidades.component.html',
  styleUrls: ['./mensalidades.component.css'],
  providers: [ UnidadeSelectedService ]
})
export class MensalidadesComponent implements OnInit {

  mensalidades : any

  public unidades : any

  public res_mensalidades : any

  public nome_unidade : string = "Escolha Unidade de Loja"

  public estadoMensalidades : boolean
  public estadoEscolhaUnidade : boolean
  public estadoLoad : boolean = false
  public mensagem_loader : string

  public dados_conta = JSON.parse(localStorage.getItem('dados_conta'))

  public colaborador = this.dados_conta.colaborador
  public id_unidade = this.colaborador[0].id_unidade
  public is_admin = this.colaborador[0].admin

  constructor(
    private requests : Requests
    ) { }

  ngOnInit() {

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      UnidadeSelected => {
        this.unidades = UnidadeSelected
        this.id_unidade = this.unidades.id
        this.nome_unidade = this.unidades.nome_fantasia
      }
    )

    this.buscarFaturasMensalidade()

    if(this.is_admin===1){
      this.estadoEscolhaUnidade = true
    }else{
      this.estadoEscolhaUnidade = false
    }

  }

  buscarFaturasMensalidade(){
    this.mostrarLoader(true, "Buscando Mensalidades")
    this.requests.solicitarFaturasMensalidade(this.id_unidade)
    .subscribe(
      res => {
        this.mostrarLoader(false, null)
        this.res_mensalidades = res
        if(this.res_mensalidades.code===200){
          this.mostrarListaMensalidade(true)
          this.mensalidades = this.res_mensalidades.data
        }else{
          this.mostrarListaMensalidade(false)
        }
      }
    )
  }

  verFatura(url_fatura){
    window.open(url_fatura, '_blank');
  }

  mostrarListaMensalidade(status : boolean){
    this.estadoMensalidades = status
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}

