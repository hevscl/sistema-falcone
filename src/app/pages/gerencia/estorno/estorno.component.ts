import { TelefoneValidation } from './../../../../utils/valida-telefone';
import { AcessPagesService } from './../../../../services/modules_services/acess_pages.service';
import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';

@Component({
  selector: 'app-estorno',
  templateUrl: './estorno.component.html',
  styleUrls: ['./estorno.component.css'],
  providers: [ 
    AcessPagesService,
    ResponsesModuleService,
    TelefoneValidation ]
})
export class EstornoComponent implements OnInit {

  public estadoAcess : boolean = false
  public estadoLoad : boolean = false
  public responseScreen : boolean = false
  public estadoLoadTable : boolean = false
  public estadoEstorno : boolean

  public mensagem_loader : string

  public msg_no_estorno : string

  public estornos : any

  public estadoForm : string = "disabled"

  public valor_trocoValido : boolean = false
  public celularValido : boolean = false
  public codigoValido : boolean = false

  public celular_mask = ['(', /\d/, /\d/,')', /\d/, /\d/,/\d/,/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  public formEstorno :FormGroup = new FormGroup({
    'valor_troco_estorno' : new FormControl(null),
    'celular' : new FormControl(null),
    'cod_transacao' : new FormControl(null),
    'mensagem' : new FormControl(null)
  })

  constructor(
    private acessPagesService : AcessPagesService,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService,
    private validaTelef : TelefoneValidation) { }

  ngOnInit() {
    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        this.mostrarResponseScreen(false, null, null, null)
      }
    )

    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Gerenciar conta",
        style: "link_blur",
        router: "gerencia"
      },
      {
        page: "Estorno",
        style: "link_ativo",
        router: "estorno"
      }]
    )

    this.listarEstornos()
  }

  solicitarEstorno(){
    var dados_estorno : any = {
      celular : this.formEstorno.value.celular,
      valor : this.formEstorno.value.valor_troco_estorno,
      codigo : this.formEstorno.value.cod_transacao,
      obs_cliente : this.formEstorno.value.mensagem
    }

    this.mostrarLoader(true, "Solicitando Estorno")

    if(this.formEstorno.value.celular!=null){
      this.requests.solicitarEstorno(dados_estorno)
      .subscribe(
        res => {
          this.mostrarLoader(false, null)
          var resp_estorno : any = res
          if(resp_estorno.code===200){
            this.formEstorno.reset()
            this.listarEstornos()
          }else{
            
          }
          this.mostrarResponseScreen(
            true,
            resp_estorno.code,
            resp_estorno.mensagem,
            "100%")
        }
      )
    }

  }

  listarEstornos(){
    this.mostrarLoad(true, "Carregando Estornos")
    this.requests.listarEstorno()
    .subscribe(
      res =>
        { 
          this.mostrarLoad(false, null)
          var resp_estorno : any = res
          if(resp_estorno.code === 200){
            this.estornos = resp_estorno.data
            this.estadoEstorno = true
          }else{
            this.estornos = {}
            this.msg_no_estorno = resp_estorno.mensagem
            this.estadoEstorno = false
          }
         }
    )
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarLoad(status :boolean, mensagem: string){
    this.estadoLoadTable = status
    this.mensagem_loader = mensagem
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  validaCelular(celular){
    this.celularValido = this.validaTelef.validarTel(celular.value)
    this.ativarBotao()
  }

  validaCodigo(codigo : string){
    if(this.formEstorno.value.cod_transacao.length>11){
      this.codigoValido = true
    }else{
      this.codigoValido = false
    }
    this.ativarBotao()
  }

  validaValorEstorno(){
    if(this.formEstorno.value.valor_troco_estorno>0 && this.formEstorno.value.valor_troco_estorno.toString().indexOf(".")=== -1){
      this.formEstorno.value.valor_troco_estorno = this.formEstorno.value.valor_troco_estorno + ".00"}
      console.log(this.formEstorno.value.valor_troco_estorno)
  }

  ativarBotao(){
    if(this.celularValido == true && this.codigoValido == true){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

}
