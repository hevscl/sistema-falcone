import { Component, OnInit } from '@angular/core';
import { BancoSelectedService } from '../../../../../services/modules_services/banco_selected.service';

@Component({
  selector: 'app-bancos',
  templateUrl: './bancos.component.html',
  styleUrls: ['./bancos.component.css'],
  providers: [BancoSelectedService]
})
export class BancosComponent implements OnInit {

  bancos : any = [
    {nome: "Bradesco S/A"},
    {nome: "Banco do Brasil S/A"},
    {nome: "Caixa Econômica Federal"},
    {nome: "Itaú Unibanco Holding S/A"}
  ]

  banco_selecionado : string

  constructor(private bancoSelectedService : BancoSelectedService) { }

  ngOnInit() {
    
  }

  mudarParametro(banco_selecionado){
    this.bancoSelectedService.enviaBancoSelecionado(banco_selecionado)
  }

}
