import { Component, OnInit } from '@angular/core';
import { UnidadeSelectedService } from '../../../../services/unidade_selected.service';
import { UnidadeSelected } from '../../../../models/unidade_selected.model';
import { Requests } from '../../../../services/requests.service';
import { Transferencia } from '../../../../models/transferencia.model';
import { FormGroup, FormControl } from '@angular/forms';
import { ErrorInputService } from '../../../../services/modules_services/error_input.service';
import { BancoSelectedService } from '../../../../services/modules_services/banco_selected.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.css'],
  providers: [UnidadeSelectedService, ErrorInputService, BancoSelectedService, ResponsesModuleService, AcessPagesService]
})
export class TransferenciaComponent implements OnInit {

  public estadoAcess : boolean = false

  estadoLoad : boolean = false
  responseScreen :boolean = false

  errors_input : boolean = false

  resposta_transf : any

  public formTransferencia :FormGroup = new FormGroup({
    'banco' : new FormControl(null),
    'agencia' : new FormControl(null),
    'conta' : new FormControl(null),
    'valor' : new FormControl(null),
    'tipo_conta' : new FormControl(null)
  })

  public tipo_conta :string

  public unidade_escolhida : UnidadeSelected

  public banco_selecionado : string = "Banco"

  public msg_tranferencia : Array<string> = ["1 - Prazo: até 3 dias úteis para efetuação do saque.",
  "2 - Sua empresa deve ser titular da conta bancária.",
  "3 - Será permitido apenas um saque gratuito por mês, os demais serão cobrados R$ 2,50 por saque.",
  "4 - O valor mínimo para saque é de R$ 20,00.",
  "5 - O valor será descontado do saldo de reserva de troco da unidade escolhida."]

  public titulo_pagina : string = "Escolha a unidade para solicitar a transferência"

  constructor(private requests :Requests, private unidadeSelectedService : UnidadeSelectedService,
    private errorMensagemService : ErrorInputService,
    private responsesModuleService : ResponsesModuleService,
    private acessPagesService : AcessPagesService ) { }

  ngOnInit() {
    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Gerenciar conta",
        style: "link_blur",
        router: "gerencia"
      },
      {
        page: "Transferência",
        style: "link_ativo",
        router: "transferencia"
      }]
    )

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidadeSelected => {
        this.unidade_escolhida = unidadeSelected
        this.mostrarErrordeInput(false)
      }
    )

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      res => {this.mostrarResponseScreen(res, null, null, "297px")}
    )

    BancoSelectedService.emitirBancoSelected.subscribe(
      banco_seleted => {this.banco_selecionado = banco_seleted}
    )

    ErrorInputService.emitirMensagemErro.subscribe(
      mensagem_error => {
        this.mostrarErrordeInput(true)
      }
    )

    UnidadeSelectedService.putTituloPagina(this.titulo_pagina)
  }

  public solicitarTransferencia() :void{
    if(this.unidade_escolhida != undefined){
      if(this.banco_selecionado != "Banco"){
        this.mostrarErrordeInput(false)

        let transferencia : Transferencia = new Transferencia(
          this.unidade_escolhida.id,
          this.banco_selecionado,
          this.formTransferencia.value.agencia,
          this.formTransferencia.value.conta,
          this.formTransferencia.value.tipo_conta,
          this.formTransferencia.value.valor
        )
        
        this.mostrarLoad(true)
        
        this.requests.solicitarTransferencia(transferencia)
        .subscribe(
          data => {
            this.mostrarLoad(false)
            this.resposta_transf = data;
            this.mostrarResponseScreen(true, this.resposta_transf.code, this.resposta_transf.mensagem, "297px")
            if(this.resposta_transf.code === 200){
              this.unidadeSelectedService.refreshUnidades(true)
            }
          }
        )

    }else{
      this.errorMensagemService.enviaMensagemErro("Você deve escolher um banco.")
    }  
    }else{
      this.errorMensagemService.enviaMensagemErro("Você deve escolher uma unidade.")
    }
  }

  mostrarLoad(status: boolean){
    this.estadoLoad = status
  }

  mostrarResponseScreen(status :boolean, code: number, mensagem: string, altura : string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarErrordeInput(status :boolean){
    this.errors_input = status
  }

}

