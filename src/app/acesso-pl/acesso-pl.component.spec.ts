import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcessoPlComponent } from './acesso-pl.component';

describe('AcessoPlComponent', () => {
  let component: AcessoPlComponent;
  let fixture: ComponentFixture<AcessoPlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcessoPlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcessoPlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
