import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router'

import { Auth } from '../../../services/auth.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-login-pl',
  templateUrl: './login-pl.component.html',
  styleUrls: ['./login-pl.component.css']
})
export class LoginPlComponent implements OnInit {

  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter()
  @ViewChild('swalWarning' ) private atencaoSwal: SwalComponent

  public formLogarPL: FormGroup = new FormGroup({
    'cpf' : new FormControl(null, [ Validators.required, Validators.minLength(11), Validators.maxLength(11) ]),
    'senha' : new FormControl(null)
  })

  public estadoLoad :boolean = false

  public resposta_dados: any = [];
  public dados_conta: any = [];
  public sessaotoken: string = '';

  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]

  constructor(
    private auth: Auth,
    private router: Router
  ) { }

  ngOnInit() {
  }

  public exibirPainelCadastro(): void{
    this.exibirPainel.emit('cadastro')
  }

  public logarPL(): void {
    this.abrirLoad(true)
    this.auth.loginPL(this.formLogarPL.value.cpf, this.formLogarPL.value.senha)
    .subscribe(
      data => {
          this.abrirLoad(false)
          if(data != null){
            this.resposta_dados = data;
            if(this.resposta_dados.code === 200){

              localStorage.setItem('auth_token', this.resposta_dados.token);

              this.dados_conta = this.resposta_dados.data
              
              localStorage.setItem('dados_conta', JSON.stringify(this.dados_conta))

              this.router.navigate(['/dashboard']);
              
            }else{
              this.atencaoSwal.show()
            }
          }
      })
  }

  public abrirLoad(status :boolean) :void{
      this.estadoLoad = status;
  }
}
