import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcessPagesComponent } from './acess-pages.component';

describe('AcessPagesComponent', () => {
  let component: AcessPagesComponent;
  let fixture: ComponentFixture<AcessPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcessPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcessPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
