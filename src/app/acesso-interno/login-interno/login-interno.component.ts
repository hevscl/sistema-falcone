import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router'

import { Auth } from '../../../services/auth.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { Unidades } from '../../../models/unidade.model';
import { TogglePasswordService } from 'src/services/modules_services/toggle_password.service';

@Component({
  selector: 'app-login-interno',
  templateUrl: './login-interno.component.html',
  styleUrls: ['./login-interno.component.css'],
  providers: [ TogglePasswordService ]
})
export class LoginInternoComponent implements OnInit {
  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter()
  @ViewChild('swalWarning' ) private atencaoSwal: SwalComponent

  public formLogarPJ: FormGroup = new FormGroup({
    'cpf' : new FormControl(null, [ Validators.required, Validators.minLength(11), Validators.maxLength(11) ]),
    'senha' : new FormControl(null)
  })

  unidades : Unidades

  public estadoLoad :boolean = false

  public show_password : boolean = false
  public togglePassword : boolean = false
  public text_toggle : string = "Mostrar"

  public resposta_dados: any = [];
  public resposta_dados_unidades: any = [];
  public dados_conta: any = [];
  public sessaotoken: string = '';

  public contador : string

  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]

  constructor(
    private auth: Auth,
    private router: Router,
    private togglePasswordService : TogglePasswordService
  ) { }

  ngOnInit() {
  }

  public exibirPainelCadastro(): void{
    this.exibirPainel.emit('cadastro')
  }

  public logarPJ(): void {
    this.abrirLoad(true)
    this.auth.loginColaborador(this.formLogarPJ.value.cpf, this.formLogarPJ.value.senha)
    .subscribe(
      data => {
          this.abrirLoad(false)
          if(data != null){
            this.resposta_dados = data;
            if(this.resposta_dados.code === 200){

              localStorage.setItem('auth_token', this.resposta_dados.token);

              this.dados_conta = this.resposta_dados.data
              
              localStorage.setItem('dados_conta', JSON.stringify(this.dados_conta))

              this.router.navigate(['/dashboard']);
              
            }else{
              this.atencaoSwal.show()
            }
          }

      })
  }

  mostrarTogglePassword(){
    if(this.formLogarPJ.value.senha.length>0){
      this.togglePassword = true
    }else{
      this.togglePassword = false
    }
  }

  mostrarPassword(){
    this.show_password = !this.show_password
    if(this.show_password){
      this.togglePasswordService.enviaMostrarPassword(true)
    }else{
      this.togglePasswordService.enviaMostrarPassword(false)
    }
  }

  public abrirLoad(status :boolean) :void{
      this.estadoLoad = status;
  }

  public irParaLoginPl(){
    this.router.navigate(['/login-pl'])
  }

}